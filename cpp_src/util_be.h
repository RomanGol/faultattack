/*

Filename:		util_be.h
Function:		some utilities to do crypto operations
Maintainer:		RomanGol  romangoliard@gmail.com
Last Modify:	2011-11
Comment:		Big Endian version

*/

#ifndef _UTIL_BE_H_
#define _UTIL_BE_H_

#include <cstdlib>
#include <cstdio>
#include <vector>
#include "dfa.h"


namespace triod
{
	template<size_t Length>
	extern bool Compare_section( const unsigned char (&lhs)[Length], const unsigned char (&rhs)[Length] );

	template<size_t Length>
	extern void Bit_permutation ( const unsigned char (&input)[Length], unsigned char (&output)[Length], const unsigned char (&ptable)[Length * 8] );

	template<size_t Block_size>
	extern void Rotate_key ( Rotate_type types, unsigned char (&input)[Block_size], size_t move_bits );

	template<size_t Block_size>
	extern void Print_block( const unsigned char (&text)[Block_size] );

	template<size_t Block_size>
	extern void Rotate_block ( Rotate_type types, unsigned char (&input)[Block_size], size_t move_bits );


} // End of Namespace

namespace triod
{
	/**************************************************************
	Template:	template<size_t Block_size>，Block_size表示block的字节长度
	Func name:	void Rotate_block ( Rotate_type types, unsigned char (&input)[Block_size], size_t move_bits )
	Usage:		用来对一个block进行循环移位, Big Endian
	Param:		types表示移位方式，由 enum Rotate_type { LEFT, RIGHT, NOP } 指定，input是输入的block，move_bits表示要移位的位数
	Return:		void，return by input param
	**************************************************************/
	template<size_t Block_size>
	void Rotate_block ( Rotate_type types, unsigned char (&input)[Block_size], size_t move_bits )
	{
		if ( NOP == types ) // add by RomanGol 2010-09-20
			return;

		unsigned char table[Block_size * 8];
		unsigned char output [Block_size];

		for ( size_t i = 0; i < Block_size; ++i )
		{
			for ( size_t j = 0; j < 8; ++j )
			{
				unsigned char c = static_cast<unsigned char>( ( (input[i] >> (7 - j) ) & 0x01 ) );

				if ( RIGHT == types )
					table[(i * 8 + j + move_bits) % sizeof(table)] = c;
				if ( LEFT == types )
					table[(i * 8 + j + sizeof(table) - move_bits) % sizeof(table)] = c;
			}
		}

		for ( size_t i = 0; i < Block_size; ++i )
		{
			output[i] = 0;
			for( size_t j = 0; j < 8; ++j )
			{
				output[i] <<= 1;
				output[i] |= table[i * 8 + j];
			}
		}

		memcpy( input, output, Block_size );
	}


/*
	template<size_t Block_size>
	void Rotate_key ( Rotate_type types, unsigned char (&input)[Block_size], size_t move_bits )
	{
		unsigned char output [Block_size];
		size_t move_bytes = move_bits / 8;
		size_t remain_bits = move_bits % 8;
		
		unsigned char higher_bit;
		unsigned char lower_bit;

		if ( LEFT == types )
		{
			memcpy ( output, input + move_bytes, Block_size - move_bytes );
			memcpy ( output + Block_size - move_bytes, input, move_bytes );

			for ( size_t i = 0; i < remain_bits; ++i )
			{
				higher_bit = (output[0] & 0x80) >> 7;	// 保存最高位
				for ( size_t j = 0; j < Block_size - 1; ++j )
				{
					output[j] <<= 1;							// 左移之后，最低位用 0 补齐
					lower_bit = (output[j + 1] & 0x80) >> 7;
					output[j] |= lower_bit;						// 0 | lower_bit = lower_bit
				}

				output[Block_size - 1] <<= 1;
				output[Block_size - 1] |= higher_bit;
			}
		}

		if ( RIGHT == types )
		{
			memcpy ( output, input + Block_size - move_bytes, move_bytes );
			memcpy ( output + move_bytes, input, Block_size - move_bytes );

			for ( size_t i = 0; i < remain_bits; ++i )
			{
				lower_bit = (output[0] & 0x01) << 7;
				higher_bit = (output[Block_size - 1] & 0x01) << 7;	// 保存最低位
				output[0] >>= 1;														// 右移之后，最高位用 0 补齐
				output[0] |= higher_bit;											// 0 | lower_bit = lower_bit

				for ( size_t j = 1; j < Block_size; ++j )
				{
					higher_bit = lower_bit;
					lower_bit = (output[j] & 0x01) << 7;
					output[j] >>= 1;								// 右移之后，最高位用 0 补齐
					output[j] |= higher_bit;					// 0 | higher_bit = higher_bit
				}
			}
		}

		if ( NOP == types )
		{
			// do nothing;
		}

		memcpy ( input, output, Block_size );
	}
*/

	/**************************************************************
	Template:		template<size_t Block_size>，Block_size表示输入block的字节长度
	Func name:	void Print_block( const unsigned char (&text)[Block_size] )
	Usage:	Print a block，每次打印一行，一行中包括这个block的每个字节的hex值（大写），每个字节间一个空格，最后打印一个换行
	Param:	text, the block to be printed
	Return:	void
	**************************************************************/
	template<size_t Block_size>
	void Print_block( const unsigned char (&text)[Block_size] )
	{
		for ( size_t i = 0; i < Block_size; ++i )
			printf( "0x%02X, ", text[i] );
		printf( "\n" );
	}


	/**************************************************************
	Template:		template<size_t Length>，Length表示输入block的字节长度
	Func name:	bool Compare_section( const unsigned char (&lhs)[Length], const unsigned char (&rhs)[Length] )
	Usage:	比较两个长度相等的unsigned char串是否内容相等
	Param:	lhs表示第一个比较的串，rhs表示第二个比较的串
	Return:	若两个串不等，返回false，两个串相等返回true
	**************************************************************/
	template<size_t Length>
	bool Compare_section( const unsigned char (&lhs)[Length], const unsigned char (&rhs)[Length] )
	{
		for ( size_t i = 0; i < Length; ++i )
		{
			if ( lhs[i] != rhs[i] )
				return false;
		}

		return true;
	}


	/**************************************************************
	Template:		template<size_t Length>，Length表示输入block的字节长度
	Func name:	void bit_permutation ( const unsigned char (&input)[Length], unsigned char (&output)[Length], const unsigned char (&ptable)[Length * 8] )
	Usage:	根据置换表ptable[i] = j，将input中的第i个bit置换到output的第j个bit上去，input和output可以是同一个块内存
				, Big Endian方式
	Param:	input是输入的待置换的unsigned char串，output是置换后的unsigned char串，ptable是置换表，len表示串的长度
	Return:	void，通过参数output传递
	**************************************************************/
	template<size_t Length>
	void Bit_permutation ( const unsigned char (&input)[Length], unsigned char (&output)[Length], const unsigned char (&ptable)[Length * 8] )
	{
		unsigned char table[Length * 8];

		for ( size_t i = 0; i < Length; ++i )
		{
			for( size_t j = 0; j < 8; ++j )
			{
				// Big Endian
				table[ ptable[sizeof(table) - 1 - (i * 8 + j)] ] = static_cast<unsigned char>( ( (input[i] >> (7 - j) ) & 0x01 ) );
			}
		}

		for ( size_t i = 0; i < Length; ++i )
		{
			output[i] = 0;
			for( size_t j = 0; j < 8; ++j )
			{
				output[i] <<= 1;
				output[i] |= table[sizeof(table) - 1 - (i * 8 + j)];
			}
		}
	}


	/**************************************************************
	Template:		template<size_t Length>，Length表示输入block的字节长度
	Func name:	void To_Bit_vector ( const unsigned char (&input)[Length] )
	Usage:	将一个unsigned char序列转换成对应的二进制序列，按照Big Endian方式排列
	Param:	input是输入的待置换的unsigned char序列
	Return:	vector<unsigned char>
	**************************************************************/
	template<size_t Length>
	std::vector<unsigned char> To_Bit_vector ( const unsigned char (&input)[Length] )
	{
		std::vector<unsigned char> table(Length * 8);

		for ( size_t i = 0; i < Length; ++i )
		{
			for( size_t j = 0; j < 8; ++j )
			{
				table[i * 8 + j] = (
										( input[i] >> (7 - j) )
										&
										0x01
									);
			}
		}
		return table;
	}

	/**************************************************************
	Func name:	std::vector<unsigned long> split_bit_vector( const std::vector<unsigned long> &v, int step )
	Usage:	将一个std::vector<unsigned char>(其中每个元素是0或者1)存贮的二进制序列按照指定步长step切割，
				默认原二进制序列按照Big Endian方式排列
				例如序列111000110001，按照步长3切割为111,000,110,001，即7,0,6,1
	Param:	v是输入的待切割的二进制序列，step是步长
	Return:	vector<unsigned long>，存贮了切割后的数值序列
	**************************************************************/
	std::vector<unsigned long> split_bit_vector( const std::vector<unsigned char> &v, int step )
	{
		static const int power[] = { 0, 1, 2, 4, 8, 16, 32, 64, 128 };
		unsigned long t;
		std::vector<unsigned long> vec;
		for ( size_t i = 0; i < v.size(); ++i )
		{
			if ( i % step == 0 )
			{
				t = 0;
				t += v[i] * power[step];
			}
			else if ( i % step == step - 1 )
			{
				t += v[i];
				vec.push_back(t);
			}
			else
			{
				t += v[i] * power[step - i % step];
			}
		}
		return vec;
	}


	/**************************************************************
	Func name:	std::vector<unsigned long> split_and_print_block( const unsigned char (&input)[Block_size], int step )
	Usage:	将一个std::vector<unsigned long>(其中每个元素是0或者1)存贮的二进制序列按照指定步长step切割，
				默认原二进制序列按照Big Endian方式排列，然后打印
				例如序列111000110001，按照步长3切割为111,000,110,001，即7,0,6,1
	Param:	input是输入的待切割的二进制序列，step是步长
	Return:	无，直接输出到std_out
	**************************************************************/
	template<size_t Block_size>
	void split_and_print_block( const unsigned char (&input)[Block_size], int step )
	{
		std::vector<unsigned long> v = split_bit_vector( triod::To_Bit_vector<Block_size>(input), step );
		for ( size_t i = 0; i < v.size(); ++i )
			printf( "0x%02lx, ", v[i] );
		printf("\n");
	}
}

#endif
