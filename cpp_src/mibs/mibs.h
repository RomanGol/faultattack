#ifndef _MIBS_H_
#define _MIBS_H_

#include <string.h>
#include "feistel.h"
#include "util_le.h"
#include "mibs_key.h"

/* ------------------------ Declaration	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class MIBS : public FEISTEL<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)		[Block_size];
		typedef unsigned char			(&block)			[Block_size];
		typedef const unsigned char		(&key_block)		[Key_size];
		typedef const unsigned char		(&const_half_block)	[Block_size / 2];
		typedef unsigned char			(&half_block)		[Block_size / 2];

		void encrypt	( const_block plain, block cipher, key_block key );
		void decrypt	( const_block cipher, block plain, key_block key );
		void encrypt_f	( const_block plain, block subkey, key_block key );
		
	protected:
		void round_func	( half_block left, half_block right );

		void key_addition	( half_block input, const_half_block key );
		void permutation	( half_block in );
		void substitution	( half_block in );


		void key_schedule	( Op_type t, key_block key );

		unsigned short	round_counter_;
		unsigned char	key_[Key_size];
		unsigned char	round_key_[Rounds + 1][Block_size / 2];	
	};
} // End of Namespace


/* ------------------------ Implementation 	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MIBS<Block_size, Key_size, Rounds>::encrypt( const_block plain, block cipher, key_block key )
	{
		this->key_schedule( ENCRYPT, key );
		
		memcpy( cipher, plain, Block_size );

		unsigned char left[Block_size / 2];
		unsigned char right[Block_size / 2];
		//unsigned char rkey[Block_size / 2];
		
		for ( round_counter_ = 0; round_counter_ < Rounds; ++round_counter_ )
		{
			memcpy( right, cipher, Block_size / 2 );
			memcpy( left, cipher + Block_size / 2, Block_size / 2 );
			
			this->round_func( left, right );

			memcpy( cipher, cipher + Block_size / 2, Block_size / 2 );
			memcpy( cipher + Block_size / 2, right, Block_size / 2 );
		}

		//memcpy( left, cipher, Block_size / 2 );
		//memcpy( cipher, cipher + Block_size / 2, Block_size / 2 );
		//memcpy( cipher + Block_size / 2, left, Block_size / 2 );
 	}
	

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MIBS<Block_size, Key_size, Rounds>::encrypt_f( const_block plain, block cipher, key_block key )
	{
		// no implementation
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MIBS<Block_size, Key_size, Rounds>::decrypt( const_block cipher, block plain, key_block key )
	{
		// no implementation
	}	
	

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MIBS<Block_size, Key_size, Rounds>::key_addition ( half_block input, const_half_block key )
	{ // cos of Feistel structure, only half of the block is sboxed
		for ( size_t i = 0; i < sizeof(key); ++i )
			input[i] ^= key[i];
	}


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MIBS<Block_size, Key_size, Rounds>::permutation( half_block in )
	{
		unsigned char nibble[Block_size];
		unsigned char tmp[Block_size];
		for ( size_t i = 0; i < sizeof(in); ++i )
		{
			nibble[i * 2] = (in[i] & 0x0F);
			nibble[i * 2 + 1] = (in[i] >> 4);
		}

		tmp[0] = nibble[1] ^ nibble[2] ^ nibble[3] ^ nibble[4] ^ nibble[5] ^ nibble[6];
		tmp[1] = nibble[0] ^ nibble[2] ^ nibble[3] ^ nibble[5] ^ nibble[6] ^ nibble[7];
		tmp[2] = nibble[0] ^ nibble[1] ^ nibble[3] ^ nibble[4] ^ nibble[6] ^ nibble[7];
		tmp[3] = nibble[0] ^ nibble[1] ^ nibble[2] ^ nibble[4] ^ nibble[5] ^ nibble[7];
		tmp[4] = nibble[0] ^ nibble[1] ^ nibble[3] ^ nibble[4] ^ nibble[5];
		tmp[5] = nibble[0] ^ nibble[1] ^ nibble[2] ^ nibble[5] ^ nibble[6];
		tmp[6] = nibble[1] ^ nibble[2] ^ nibble[3] ^ nibble[6] ^ nibble[7];
		tmp[7] = nibble[0] ^ nibble[2] ^ nibble[3] ^ nibble[4] ^ nibble[7];

		const static int ptable[] = { 1, 7, 0, 2, 5, 6, 3, 4 };
		for ( size_t i = 0; i < Block_size; ++i)
		{
			nibble[i] = tmp[ ptable[i] ]; //nibble[ ptable[i] ] = tmp[i];
		}

		for ( size_t i = 0; i < sizeof(in); ++i )
		{
			in[i] = nibble[i * 2] + (nibble[i * 2 + 1] << 4);
		}
	}	


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MIBS<Block_size, Key_size, Rounds>::round_func	( half_block left, half_block right )
	{
		this->key_addition( left, round_key_[round_counter_] );
		this->substitution( left );
		this->permutation( left );
		this->key_addition( right, left );
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MIBS<Block_size, Key_size, Rounds>::substitution( half_block in )
	{ // cos of Feistel structure, only half of the block is sboxed
		const static unsigned char Sbox[] = { 4, 15, 3, 8, 13, 10, 12, 0, 11, 5, 7, 14, 2, 6, 1, 9 };

		for ( size_t i = 0; i < sizeof(in); ++i )
		{
			in[i] = Sbox[ in[i] & 0x0F ] + ( Sbox[in[i] >> 4] << 4 );
		}
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MIBS<Block_size, Key_size, Rounds>::key_schedule( Op_type t, key_block key )
	{
		MIBS_Key_scheduler<Block_size, Key_size, Rounds> ks;
		ks.set_key( key );
		ks.key_schedule( t );

		for ( size_t i = 0; i < Rounds; ++i )
			ks.fill_round_key( i, round_key_[i] );
	}
} // End of Namespace
#endif
