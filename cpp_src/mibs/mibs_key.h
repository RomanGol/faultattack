/*

Filename:	mibs_key.h
Function:		MIBS Key Scheduler
Maintainer:	RomanGol  romangoliard@gmail.com
Last Modify:	2010-10
Comment:		

*/

#ifndef _MIBS_KEYSCHEDR_H_
#define _MIBS_KEYSCHEDR_H_

#include "keyschedr_feistel.h"
#include "aria.h"


/* ------------------------ Declaration	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class MIBS_Key_scheduler : public Key_scheduler_feistel<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)		[Block_size];
		typedef unsigned char			(&block)			[Block_size];
		typedef const unsigned char		(&key_block)		[Key_size];
		typedef const unsigned char		(&const_half_block)	[Block_size / 2];
		typedef unsigned char			(&half_block)		[Block_size / 2];

		void fill_round_key	( size_t nth_round, half_block round_key );
		void set_key		( key_block key );
		void key_schedule	( Op_type t );

	protected:
		unsigned char	key_[Key_size];
		unsigned char	round_key_[Rounds][Block_size / 2];

		unsigned char encrypt_round_key_[Rounds][Block_size / 2];
		unsigned char decrypt_round_key_[Rounds][Block_size / 2];
	}; // End of class SPN


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MIBS_Key_scheduler<Block_size, Key_size, Rounds>::key_schedule( Op_type t )
	{
		const static unsigned char Sbox[] = { 4, 15, 3, 8, 13, 10, 12, 0, 11, 5, 7, 14, 2, 6, 1, 9 };		

		if ( ENCRYPT == t )
		{
			if ( Key_size == 8 )
			{
				memcpy( encrypt_round_key_[0], key_ + Block_size / 2, Block_size / 2 );
				memcpy( round_key_[0], key_ + Block_size / 2, Block_size / 2 );

				for ( size_t i = 1; i < Rounds; ++i )
				{
					Rotate_block<Key_size>( RIGHT, key_, 15 );
					key_[7] = (Sbox[ key_[7] >> 4 ] << 4) + (key_[7] & 0x0F);
					key_[1] ^= (i << 3);
					
					memcpy( encrypt_round_key_[i], key_ + Block_size / 2, Block_size / 2 );
					memcpy( round_key_[i], key_ + Block_size / 2, Block_size / 2 );
				}
			}
			if ( Key_size == 10 )
			{
				memcpy( encrypt_round_key_[0], key_ + Block_size / 2, Block_size / 2 );
				memcpy( round_key_[0], key_ + Block_size / 2, Block_size / 2 );

				for ( size_t i = 1; i < Rounds; ++i )
				{
					Rotate_block<Key_size>( RIGHT, key_, 19 );
					key_[9] = Sbox[ key_[9] >> 4 ] * 0x10 + Sbox[(key_[9] & 0x0F)];
					key_[1] ^= (i << 6);
					key_[2] ^= (i >> 2);

					memcpy( encrypt_round_key_[i], key_ + Block_size / 2, Block_size / 2 );
					memcpy( round_key_[i], key_ + Block_size / 2, Block_size / 2 );
				}
			}
		}
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MIBS_Key_scheduler<Block_size, Key_size, Rounds>::set_key( key_block key )
	{
		memcpy( key_, key, sizeof(key) );
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MIBS_Key_scheduler<Block_size, Key_size, Rounds>::fill_round_key( size_t nth_round, half_block round_key )
	{
		// feistel round key has half size of Block
		memcpy( round_key, round_key_[nth_round], Block_size / 2 );
	}

} // End of Namespace




#endif
