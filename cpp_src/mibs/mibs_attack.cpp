#include <iostream>
#include <ctime>
#include <cstdlib>
#include "present.h"
#include "present_fault.h"
#include "printcipher.h"
#include "printcipher_fault.h"
#include "aria.h"
#include "puffin.h"
#include "mibs.h"
#include "aes.h"
#include "util_le.h"

int mibs()
{
	unsigned char key[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	unsigned char text[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	unsigned char cipher[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	triod::MIBS<sizeof(text), sizeof(key), 32> roman;
	
	roman.encrypt( text, cipher, key );
	triod::Print_block<sizeof(cipher)>( cipher ); // 6D1D3722  E19613D2
	return 0;
}
