/*

文件名：	CAMELLIA_key.h
功能：		CAMELLIA Key Scheduler
维护者：	RomanGol  romangoliard@gmail.com
最后维护：	2011-03
说明：		

*/

#ifndef _CAMELLIA_KEYSCHEDR_H_
#define _CAMELLIA_KEYSCHEDR_H_

#include "keyschedr.h"
#include "CAMELLIA_const.h"

/* ------------------------ Declaration	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class CAMELLIA_Key_scheduler : Key_scheduler<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)	[Block_size];
		typedef unsigned char			(&block)		[Block_size];
		typedef const unsigned char		(&key_block)	[Key_size];

		void fill_round_key	( size_t nth_round, block round_key );
		void set_key		( key_block key );
		void key_schedule	( Op_type t );

	protected:
		unsigned char	key_[Key_size];
		unsigned char	round_key_[Rounds + 1][Block_size];		
	}; // End of class SPN


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void CAMELLIA_Key_scheduler<Block_size, Key_size, Rounds>::key_schedule( Op_type t )
	{
		unsigned char t[64];
		unsigned long u[20];

		memcpy( t, key_, Key_size );
		if( Key_size == 128/8 )
			for( size_t i=16; i<32; i++ ) t[i] = 0;
		else if( Key_size == 192/8 )
			for( size_t i=24; i<32; i++ ) t[i] = key_[i-8]^0xff;

		XorBlock( t+0, t+16, t+32 );

		Camellia_Feistel( t+32, SIGMA+0, t+40 );
		Camellia_Feistel( t+40, SIGMA+8, t+32 );

		XorBlock( t+32, t+0, t+32 );

		Camellia_Feistel( t+32, SIGMA+16, t+40 );
		Camellia_Feistel( t+40, SIGMA+24, t+32 );

		ByteWord( t+0,  u+0 );
		ByteWord( t+32, u+4 );

		if( n == 128 )
		{
			for( i=0; i<26; i+=2 )
			{
				RotBlock( u+KIDX1[i+0], KSFT1[i+0], u+16 );
				RotBlock( u+KIDX1[i+1], KSFT1[i+1], u+18 );
				WordByte( u+16, e+i*8 );
			}
		}
		else
		{
			XorBlock( t+32, t+16, t+48 );

			Camellia_Feistel( t+48, SIGMA+32, t+56 );
			Camellia_Feistel( t+56, SIGMA+40, t+48 );

			ByteWord( t+16, u+8  );
			ByteWord( t+48, u+12 );

			for( i=0; i<34; i+=2 )
			{
				RotBlock( u+KIDX2[i+0], KSFT2[i+0], u+16 );
				RotBlock( u+KIDX2[i+1], KSFT2[i+1], u+18 );
				WordByte( u+16, e+(i<<3) );
			}
		}
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void CAMELLIA_Key_scheduler<Block_size, Key_size, Rounds>::set_key( key_block key )
	{
		for ( size_t i = 0; i < Key_size; ++i )
			key_[i] = key[i];
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void CAMELLIA_Key_scheduler<Block_size, Key_size, Rounds>::fill_round_key( size_t nth_round, block round_key )
	{
		memcpy( round_key, round_key_[nth_round], Block_size );
	}

} // End of Namespace




#endif