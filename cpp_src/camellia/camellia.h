/*

文件名：	CAMELLIA.h
功能：		分组加密算法CAMELLIA
维护者：	RomanGol  romangoliard@gmail.com
最后维护：	2011-03
说明：		由于使用了模版，无法分离编译……
修订说明：	2011年03月，新建

*/

#ifndef _CAMELLIA_H_
#define _CAMELLIA_H_

#include <cstdio>
#include <ctime>
#include <cstring>
#include <set>
#include <vector>

#include "spn.h"
#include "util.h"
#include "CAMELLIA_key.h"


#define IMPORT_ERROR 1

namespace triod
{
	// implementation starts here
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class CAMELLIA : SPN<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)	[Block_size];
		typedef unsigned char			(&block)		[Block_size];

		CAMELLIA();
		
		void encrypt( const_block plain, block cipher, key_block key );
		void decrypt( const_block cipher, block plain, key_block key );
		void set_key( key_block key );

		void encrypt_f( const_block plain, block cipher, key_block key );

		void encrypt( const_block text, block cipher );
		void decrypt( const_block cipher, block text );


		void key_addition	( block input, const_block key );

	/* ---------------------------------------------------------------------------------------- */
	protected:
		void substitution			( block in );
		void permutation			( block in );
		void shift_rows				( block in );
		void mix_columns			( block in );

		void key_schedule			( Op_type t, key_block key ); // 密钥编排
		unsigned char encrypt_round_key_[Rounds][Block_size];
		unsigned char decrypt_round_key_[Rounds][Block_size];
	};
}

/* -------------------------------------------------------------------------------------------------------------------------------- */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	CAMELLIA<Block_size, Key_size, Rounds>::CAMELLIA()
	{
		// 一个小小技巧，用于在编译期防止 Key_size 非法
		char guard[(Key_size == 16 || Key_size == 24 || Key_size == 32) ? 1 : -1] = { 0 };


		// 一个小小技巧，用于在编译期防止 Rounds 非法
		char guard_a[(Rounds == 10 || Rounds == 11 || Rounds == 12) ? 1 : -1] = { 0 };
	}


	
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void CAMELLIA<Block_size, Key_size, Rounds>::encrypt( const_block plain, block cipher, key_block key )	// 注意Rounds比实际的加密需要的轮数少一轮，用于最后一步的不做diffuse的运算
	{
		memcpy( cipher, plain, Block_size );
		this->key_schedule( ENCRYPT, key );
		
		this->key_addition( cipher, this->round_key_[0] );

		for ( round_counter_ = 1; round_counter_ < Rounds; ++round_counter_ )
		{
			this->substitution( cipher );
			
			this->permutation( cipher );
			this->key_addition( cipher, round_key_[round_counter_] );
			
		}

		// last round
		this->substitution( cipher );
		this->shift_rows( cipher );
		this->key_addition( cipher, round_key_[Rounds] );
		triod::Print_block<Block_size>( cipher );
		
		round_counter_ = 0;
	}

	
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void CAMELLIA<Block_size, Key_size, Rounds>::decrypt( const_block cipher, block plain, key_block key )
	{
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void CAMELLIA<Block_size, Key_size, Rounds>::encrypt_f( const_block plain, block cipher, key_block key )
	{
		size_t ERROR_PLACE = 7;
		size_t error_pos = 0;
		static unsigned char error = 0;

		memcpy( cipher, plain, Block_size );
		this->key_schedule( ENCRYPT, key );
		
		this->key_addition( cipher, this->round_key_[0] );

		for ( round_counter_ = 1; round_counter_ < Rounds; ++round_counter_ )
		{
			this->substitution( cipher );

			if ( round_counter_ == ERROR_PLACE )
			{
				cipher[error_pos] ^= ++error;
				//printf( "error%02x\n", cipher[error_pos] );
			}
			this->permutation( cipher );
			this->key_addition( cipher, round_key_[round_counter_] );
			
		}

		// last round
		this->substitution( cipher );
		this->shift_rows( cipher );
		this->key_addition( cipher, round_key_[Rounds] );
		
		triod::Print_block<Block_size>( cipher );
		round_counter_ = 0;
	}





/* ---------------------------------------------------------------------------------------- */
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void CAMELLIA<Block_size, Key_size, Rounds>::substitution ( block in )
	{
		for( size_t i = 0; i < Block_size; ++i )
			in[i] = Sbox[ in[i] ] ;
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void CAMELLIA<Block_size, Key_size, Rounds>::permutation ( block in )
	{
		this->shift_rows( in );
		this->mix_columns( in );
	}


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void CAMELLIA<Block_size, Key_size, Rounds>::mix_columns( block in ) 
	{
		unsigned char b[Block_size];
		size_t row = 4;
		size_t col = Block_size / 4;

		for ( size_t i = 0; i < row; ++i )
		{
			b[i * col + 0] = mul( 2, in[i * col + 0] ) ^ mul( 3, in[i * col + 1] ) ^ in[i * col + 2] ^ in[i * col + 3];
			b[i * col + 1] = mul( 2, in[i * col + 1] ) ^ mul( 3, in[i * col + 2] ) ^ in[i * col + 3] ^ in[i * col + 0];
			b[i * col + 2] = mul( 2, in[i * col + 2] ) ^ mul( 3, in[i * col + 3] ) ^ in[i * col + 0] ^ in[i * col + 1];
			b[i * col + 3] = mul( 2, in[i * col + 3] ) ^ mul( 3, in[i * col + 0] ) ^ in[i * col + 1] ^ in[i * col + 2];
		}

		for ( size_t i = 0; i < Block_size; ++i )
			in[i] = b[i];
	}


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void CAMELLIA<Block_size, Key_size, Rounds>::shift_rows( block in ) 
	{
		const static unsigned char shifts[Block_size] = { 0, 5, 10, 15, 4, 9, 14, 3, 8, 13, 2, 7, 12, 1, 6, 11 }; 
		unsigned char tmp[Block_size];

		for( size_t i = 1; i < Block_size; ++i)
			tmp[i] = in[ shifts[i] ];
		for( size_t i = 1; i < Block_size; ++i)
			in[i] = tmp[i];
	}


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void CAMELLIA<Block_size, Key_size, Rounds>::key_schedule ( Op_type t, key_block key ) // 密钥编排
	{
		CAMELLIA_Key_scheduler<Block_size, Key_size, Rounds> ks;
		ks.set_key( key );
		ks.key_schedule( t );

		for ( size_t i = 0; i < Rounds + 1; ++i )
			ks.fill_round_key( i, round_key_[i] );
	}
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void CAMELLIA<Block_size, Key_size, Rounds>::set_key( key_block key )
	{
		for ( size_t i = 0; i < Key_size; ++i )
			key_[i] = key[i];
	}



	/* ------------------- 下面是一些针对内存块的功能函数 ------------------- */
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void CAMELLIA<Block_size, Key_size, Rounds>::key_addition( block input, const_block key )	// 针对 128 bit 的序列专门设计的异或函数
	{
		for ( size_t i = 0; i < Key_size; ++i )
			input[i] = input[i] ^ key[i];
	}


/* -------------------------------------------------------------------------------------------------------------------------------- */
}


#endif
