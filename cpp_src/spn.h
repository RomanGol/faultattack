/*

Filename:	spn.h
Function:		substitution-permutation network
Maintainer:	RomanGol  romangoliard@gmail.com
Last Modify:	2011-11
Comment:		no implementation, only declaration��no data member

*/

#ifndef _SPN_H_
#define _SPN_H_

#include "dfa.h"

/* ------------------------ Declaration	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class SPN
	{
	public:
		typedef const unsigned char		(&const_block)	[Block_size];
		typedef unsigned char			(&block)		[Block_size];
		typedef const unsigned char		(&key_block)	[Key_size];

		virtual void encrypt( const_block plain, block cipher, key_block key ) = 0;
		virtual void decrypt( const_block cipher, block plain, key_block key ) = 0;

	protected:
		virtual void key_addition	( block input, const_block key )	= 0;
		virtual void key_schedule	( Op_type t, key_block key )		= 0;
		virtual void permutation	( block in )						= 0;
		virtual void substitution	( block in )						= 0;

	}; // End of class SPN
	
} // End of Namespace




#endif
