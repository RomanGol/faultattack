/*

Filename:		keyschedr_feistel.h
Function:		Feistel Key Scheduler abstract superclass
Maintainer:		RomanGol  romangoliard@gmail.com
Last Modify:	2011-11
Comment:		no implementation, only declaration

*/

#ifndef _KEYSCHEDR_FEISTEL_H_
#define _KEYSCHEDR_FEISTEL_H_

#include "spn.h"

/* ------------------------ Declaration	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class Key_scheduler_feistel
	{
	public:
		typedef const unsigned char		(&const_block)		[Block_size];
		typedef unsigned char			(&block)			[Block_size];
		typedef const unsigned char		(&key_block)		[Key_size];
		typedef const unsigned char		(&const_half_block)	[Block_size / 2];
		typedef unsigned char			(&half_block)		[Block_size / 2];

		virtual void fill_round_key	( size_t nth_round, half_block key ) = 0;
		virtual void set_key		( key_block key ) = 0;
		virtual void key_schedule	( Op_type t ) = 0;

	protected:
		unsigned char	key_[Key_size];
		unsigned char	round_key_[Rounds + 1][Block_size];		
	}; // End of class Key_scheduler_feistel
	
} // End of Namespace




#endif