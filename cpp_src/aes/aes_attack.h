
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void AES<Block_size, Key_size, Rounds>::encrypt_f( const_block plain, block cipher, key_block key, const size_t e_round, const size_t e_pos, const unsigned char err )
	{
		memcpy( cipher, plain, Block_size );
		this->key_schedule( ENCRYPT, key );
		
		this->key_addition( cipher, this->round_key_[0] );

		for ( round_counter_ = 1; round_counter_ < Rounds; ++round_counter_ )
		{
			if ( round_counter_ == e_round )
			{
				cipher[e_pos] ^= err;
				//printf( "error%02x\n", cipher[error_pos] );
			}

			this->substitution( cipher );
			this->permutation( cipher );
			this->key_addition( cipher, round_key_[round_counter_] );
		}

		// last round
		this->substitution( cipher );
		this->shift_rows( cipher );
		this->key_addition( cipher, round_key_[Rounds] );
		
		triod::Print_block<Block_size>( cipher );
		round_counter_ = 0;
	}

