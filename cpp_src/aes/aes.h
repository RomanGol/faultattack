/*

Filename:	aes.h
Function:		block cipher AES
Maintainer:	RomanGol  romangoliard@gmail.com
Last Modify:	2011-03
Comment:		due to template��cannot compiled seperately ����
				2011-03 new

*/

#ifndef _AES_H_
#define _AES_H_

#include <cstdio>
#include <ctime>
#include <vector>

#include "../spn.h"
#include "aes_key.h"


#define IMPORT_ERROR 1

namespace triod
{
	// implementation starts here
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class AES : public SPN<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)	[Block_size];
		typedef unsigned char			(&block)		[Block_size];
		typedef const unsigned char		(&key_block)	[Key_size];

		AES();
		
		void encrypt( const_block plain, block cipher, key_block key );
		void decrypt( const_block cipher, block plain, key_block key );
		void set_key( key_block key );

		void encrypt( const_block text, block cipher );
		void decrypt( const_block cipher, block text );


		void key_addition	( block input, const_block key );

	/* ---------------------------------------------------------------------------------------- */
	protected:
		void substitution			( block in );
		void permutation			( block in );
		void shift_rows				( block in );
		void mix_columns			( block in );

		void 			key_schedule			( Op_type t, key_block key ); 
		unsigned char	encrypt_round_key_[Rounds][Block_size];
		unsigned char	decrypt_round_key_[Rounds][Block_size];

		unsigned short	round_counter_;
		unsigned char	key_[Key_size];
		unsigned char	round_key_[Rounds + 1][Block_size];		
	};
}

/* -------------------------------------------------------------------------------------------------------------------------------- */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	AES<Block_size, Key_size, Rounds>::AES()
	{
		char guard[(Key_size == 16 || Key_size == 24 || Key_size == 32) ? 1 : -1] = { 0 };
		char guard_a[(Rounds == 10 || Rounds == 11 || Rounds == 12) ? 1 : -1] = { 0 };
	}


	
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void AES<Block_size, Key_size, Rounds>::encrypt( const_block plain, block cipher, key_block key )
	{
		memcpy( cipher, plain, Block_size );
		this->key_schedule( ENCRYPT, key );

		this->key_addition( cipher, this->round_key_[0] );
		for ( round_counter_ = 1; round_counter_ < Rounds; ++round_counter_ )
		{
			this->substitution( cipher );
			this->permutation( cipher );
			this->key_addition( cipher, this->round_key_[round_counter_] );
		}

		// last round
		this->substitution( cipher );
		this->shift_rows( cipher );
		this->key_addition( cipher, round_key_[Rounds] );
		
		round_counter_ = 0;
	}

	
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void AES<Block_size, Key_size, Rounds>::decrypt( const_block cipher, block plain, key_block key )
	{
	}




/* ---------------------------------------------------------------------------------------- */
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void AES<Block_size, Key_size, Rounds>::substitution ( block in )
	{
		for( size_t i = 0; i < Block_size; ++i )
			in[i] = Sbox[ in[i] ] ;
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void AES<Block_size, Key_size, Rounds>::permutation ( block in )
	{
		this->shift_rows( in );
		this->mix_columns( in );
	}


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void AES<Block_size, Key_size, Rounds>::mix_columns( block in ) 
	{
		unsigned char b[Block_size];
		size_t row = 4;
		size_t col = Block_size / 4;

		for ( size_t i = 0; i < row; ++i )
		{
			b[i * col + 0] = mul( 2, in[i * col + 0] ) ^ mul( 3, in[i * col + 1] ) ^ in[i * col + 2] ^ in[i * col + 3];
			b[i * col + 1] = mul( 2, in[i * col + 1] ) ^ mul( 3, in[i * col + 2] ) ^ in[i * col + 3] ^ in[i * col + 0];
			b[i * col + 2] = mul( 2, in[i * col + 2] ) ^ mul( 3, in[i * col + 3] ) ^ in[i * col + 0] ^ in[i * col + 1];
			b[i * col + 3] = mul( 2, in[i * col + 3] ) ^ mul( 3, in[i * col + 0] ) ^ in[i * col + 1] ^ in[i * col + 2];
		}

		memcpy( in, b, Block_size );
	}


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void AES<Block_size, Key_size, Rounds>::shift_rows( block in ) 
	{
		const static unsigned char shifts[Block_size] = { 0, 5, 10, 15, 4, 9, 14, 3, 8, 13, 2, 7, 12, 1, 6, 11 }; 
		unsigned char tmp[Block_size];

		for( size_t i = 0; i < Block_size; ++i)
			tmp[i] = in[ shifts[i] ];
		memcpy( in, tmp, Block_size );
	}


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void AES<Block_size, Key_size, Rounds>::key_schedule ( Op_type t, key_block key ) 
	{
		AES_Key_scheduler<Block_size, Key_size, Rounds> ks;
		ks.set_key( key );
		ks.key_schedule( t );

		for ( size_t i = 0; i < Rounds + 1; ++i )
			ks.fill_round_key( i, round_key_[i] );
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void AES<Block_size, Key_size, Rounds>::set_key( key_block key )
	{
		for ( size_t i = 0; i < Key_size; ++i )
			key_[i] = key[i];
	}



	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void AES<Block_size, Key_size, Rounds>::key_addition( block input, const_block key )
	{
		for ( size_t i = 0; i < Key_size; ++i )
			input[i] = input[i] ^ key[i];
	}


/* -------------------------------------------------------------------------------------------------------------------------------- */
}


#endif

