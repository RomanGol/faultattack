/*

Filename:	AES_key.h
Function:		AES Key Scheduler
Maintainer:	RomanGol  romangoliard@gmail.com
Last Modify:	2011-03
Comment:		

*/

#ifndef _AES_KEYSCHEDR_H_
#define _AES_KEYSCHEDR_H_

#include "../keyschedr_spn.h"
#include "aes_const.h"

/* ------------------------ Declaration	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class AES_Key_scheduler : public Key_scheduler<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)	[Block_size];
		typedef unsigned char			(&block)		[Block_size];
		typedef const unsigned char		(&key_block)	[Key_size];

		void fill_round_key	( size_t nth_round, block b );
		void set_key		( key_block key );
		void key_schedule	( Op_type t );

	protected:
		unsigned char	key_[Key_size];
		unsigned char	round_key_[Rounds + 1][Block_size];		
	}; // End of class SPN


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void AES_Key_scheduler<Block_size, Key_size, Rounds>::key_schedule( Op_type t )
	{
		uint32_t temp;
		uint32_t round_key[Block_size * (Rounds + 1) / sizeof(uint32_t)];


		memset( round_key_, 0, Block_size * (Rounds+1) );

		for ( size_t i = 0; i < Key_size / sizeof(uint32_t); ++i )
		{
			round_key[i] = key_[i*4 + 0] * 0x1000000 + key_[i*4 + 1] * 0x10000 + key_[i*4 + 2] * 0x100 + key_[i*4 + 3];
		}

		if ( Key_size == 128 / 8 ) 
		{
			for ( size_t i = 0; i < 10; ++i )
			{
				temp = round_key[i*4 + 3];
				round_key[i*4 + 4] = round_key[i*4 + 0] ^ rcon[i] ^ 
					(Te2[(temp >> 16) & 0xff] & 0xff000000) ^
					(Te3[(temp >>  8) & 0xff] & 0x00ff0000) ^
					(Te0[(temp      ) & 0xff] & 0x0000ff00) ^
					(Te1[(temp >> 24) & 0xff] & 0x000000ff) ;
				round_key[i*4 + 5] = round_key[i*4 + 1] ^ round_key[i*4 + 4];
				round_key[i*4 + 6] = round_key[i*4 + 2] ^ round_key[i*4 + 5];
				round_key[i*4 + 7] = round_key[i*4 + 3] ^ round_key[i*4 + 6];
			}
		}
		

		if ( Key_size == 192 / 8 ) 
		{
			for ( size_t i = 0; i < 8; ++i )
			{
				temp = round_key[i*6 +  5];
				round_key[i*6 +  6] = round_key[i*6 +  0] ^
					(Te2[(temp >> 16) & 0xff] & 0xff000000) ^
					(Te3[(temp >>  8) & 0xff] & 0x00ff0000) ^
					(Te0[(temp      ) & 0xff] & 0x0000ff00) ^
					(Te1[(temp >> 24) & 0xff] & 0x000000ff) ^ rcon[i];
				round_key[i*6 +  7] = round_key[i*6 +  1] ^ round_key[i*6 +  6];
				round_key[i*6 +  8] = round_key[i*6 +  2] ^ round_key[i*6 +  7];
				round_key[i*6 +  9] = round_key[i*6 +  3] ^ round_key[i*6 +  8];
				round_key[i*6 + 10] = round_key[i*6 +  4] ^ round_key[i*6 +  9];
				round_key[i*6 + 11] = round_key[i*6 +  5] ^ round_key[i*6 + 10];
			}
		}
		if ( Key_size == 256 / 8 ) 
		{
			for ( size_t i = 0; i < 7; ++i )
			{
				temp = round_key[i*8 +  7];
				round_key[i*8 +  8] = round_key[i*8 +  0] ^
					(Te2[(temp >> 16) & 0xff] & 0xff000000) ^
					(Te3[(temp >>  8) & 0xff] & 0x00ff0000) ^
					(Te0[(temp      ) & 0xff] & 0x0000ff00) ^
					(Te1[(temp >> 24) & 0xff] & 0x000000ff) ^ rcon[i];
				round_key[i*8 +  9] = round_key[i*8 +  1] ^ round_key[i*8 +  8];
				round_key[i*8 + 10] = round_key[i*8 +  2] ^ round_key[i*8 +  9];
				round_key[i*8 + 11] = round_key[i*8 +  3] ^ round_key[i*8 + 10];

				temp = round_key[i*8 + 11];
				round_key[i*8 + 12] = round_key[i*8 +  4] ^
					(Te2[(temp >> 24) & 0xff] & 0xff000000) ^
					(Te3[(temp >> 16) & 0xff] & 0x00ff0000) ^
					(Te0[(temp >>  8) & 0xff] & 0x0000ff00) ^
					(Te1[(temp      ) & 0xff] & 0x000000ff);
				round_key[i*8 + 13] = round_key[i*8 +  5] ^ round_key[i*8 + 12];
				round_key[i*8 + 14] = round_key[i*8 +  6] ^ round_key[i*8 + 13];
				round_key[i*8 + 15] = round_key[i*8 +  7] ^ round_key[i*8 + 14];
			}
		}

		memcpy( round_key_[0], key_, Block_size );
		for ( size_t i = 1; i < Rounds + 1; ++i )
		{
			memcpy( round_key_[i], round_key + i * Block_size / sizeof(uint32_t), Block_size );
			for ( size_t j = 0; j < Block_size / 4; ++j )
			{
				unsigned char swp;
				swp = round_key_[i][j * 4 + 0];
				round_key_[i][j * 4 + 0] = round_key_[i][j * 4 + 3];
				round_key_[i][j * 4 + 3] = swp;
				
				swp = round_key_[i][j * 4 + 1];
				round_key_[i][j * 4 + 1] = round_key_[i][j * 4 + 2];
				round_key_[i][j * 4 + 2] = swp;
				   		
				/*
				round_key_[i][j * 4 + 0] ^= round_key_[i][j * 4 + 3] ^= round_key_[i][j * 4 + 0] ^= round_key_[i][j * 4 + 3];
				round_key_[i][j * 4 + 1] ^= round_key_[i][j * 4 + 2] ^= round_key_[i][j * 4 + 1] ^= round_key_[i][j * 4 + 2];
				*/
			}
		}
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void AES_Key_scheduler<Block_size, Key_size, Rounds>::set_key( key_block key )
	{
		for ( size_t i = 0; i < Key_size; ++i )
			key_[i] = key[i];
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void AES_Key_scheduler<Block_size, Key_size, Rounds>::fill_round_key( size_t nth_round, block b )
	{
		memcpy( b, round_key_[nth_round], Block_size );
	}

} // End of Namespace




#endif
