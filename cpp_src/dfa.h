#ifndef _DFA_H_
#define _DFA_H_

// #include <inttypes.h> // to add uintXX_t types
typedef unsigned int uint32_t;
typedef unsigned short uint16_t;
typedef unsigned char uint8_t;

namespace triod
{
	enum Op_type { ENCRYPT, DECRYPT };

	enum Rotate_type { LEFT, RIGHT, NOP };
}
#endif

