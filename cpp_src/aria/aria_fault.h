/*

Filename:		aria_fault.h
Function:		block cipher ARIA Fault Attacker
Maintainer:		RomanGol  romangoliard@gmail.com
Last Modify:	2011-11
Comment:		due to template��cannot compiled seperately ����


*/

#ifndef _ARIA_FAULT_H_
#define _ARIA_FAULT_H_

#include "aria.h"
namespace triod
{
	// implementation starts here
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class ARIA_FAULT : public ARIA<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)	[Block_size];
		typedef unsigned char			(&block)		[Block_size];
		typedef const unsigned char		(&key_block)	[Key_size];

		void encrypt_f( const_block plain, block cipher, key_block key );
	};

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void ARIA<Block_size, Key_size, Rounds>::encrypt_f( const_block plain, block cipher, key_block key )
	{
		this->key_schedule( ENCRYPT, key );
		memcpy ( cipher, plain, Block_size );
		/*======================================*/
		
		for ( size_t i = 1; i < Rounds; ++i )
		{
			this->key_addition( cipher, encrypt_round_key_[i] );
			this->substitution( cipher, i );
			
			if ( i == Rounds - 2 )
			{
				unsigned char error = static_cast<unsigned char>( rand() % 0x100 );
				size_t error_pos = rand() % Block_size;
				if ( 0 == error ) error = 0x07;
				cipher[error_pos] ^= error;
			}

			this->permutation( cipher );

			#if ARIA_TEST
			Print_block<Block_size>( cipher );
			#endif
		}

		// Last round
		key_addition( cipher, encrypt_round_key_[Rounds] );
		for ( size_t i = 0; i < Block_size; ++i )
			cipher[i] = SBOX[EVEN_LAYER_BOX[i]][cipher[i]];
		key_addition( cipher, encrypt_round_key_[Rounds + 1] );
		// Last round

		#if ARIA_TEST
		Print_block<Block_size>( cipher );
		printf( "Encrypt TEXT\n\n");
		#endif
	}

} // end of namespace triod

