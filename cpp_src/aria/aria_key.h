/*

Filename:		aria_key.h
Function:		ARIA Key Scheduler
Maintainer:		RomanGol  romangoliard@gmail.com
Last Modify:	2011-11
Comment:		

*/

#ifndef _ARIA_KEYSCHEDR_H_
#define _ARIA_KEYSCHEDR_H_

#include "keyschedr_spn.h"
#include "aria_const.h"
#include "util_le.h"

/* ------------------------ Declaration	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class ARIA_Key_scheduler : public Key_scheduler<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)	[Block_size];
		typedef unsigned char			(&block)		[Block_size];
		typedef const unsigned char		(&key_block)	[Key_size];

		void fill_round_key	( size_t nth_round, block round_key );
		void set_key		( key_block key );
		void key_schedule	( Op_type t );

		const static unsigned short NUM_OF_ROUND_KEY			= 18;
		const static unsigned short MAX_KEY_WIDTH				= 256 / 8;

	protected:
		void permutation	( block in );
		void substitution	( block in, size_t round );
		void x_or			( block in, const_block x );

		unsigned char	key_[MAX_KEY_WIDTH];
		unsigned char	round_key_[NUM_OF_ROUND_KEY][Block_size];	// Notice that this is NUM_OF_ROUND_KEY

		unsigned char encrypt_round_key_[NUM_OF_ROUND_KEY][Block_size];		// 16 encrypt_key，each 128 bit
		unsigned char decrypt_round_key_[NUM_OF_ROUND_KEY][Block_size];		// 16 decrypt，each 128 bit
	}; // End of class SPN


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void ARIA_Key_scheduler<Block_size, Key_size, Rounds>::key_schedule( Op_type t )
	{
		// 4 Ws ，each 128 bit
		unsigned char W[4][Block_size];
		
		// kR，that is the right 128 bits of main key
		// notice little endian!!!
		unsigned char KR[Block_size];
		memcpy ( KR, key_, Block_size );

		/* ------------------- init W array ------------------- */
		// W[0] = KL，that is the left 128 bits of main key
		// notice little endian!!!
		memcpy ( W[0], key_ + Block_size, Block_size );

		// using table lookup: CK[ CK_ORDER[Key_size / 10][0] ]
		// to deal with different length of keysize and CK
		memcpy( W[1], W[0], Block_size );
		this->x_or( W[1], CK[ CK_ORDER[Key_size / 10][0] ] );
		this->substitution( W[1], 1 );
		this->permutation( W[1] );
		this->x_or( W[1], KR );

		memcpy( W[2], W[1], Block_size );
		this->x_or( W[2], CK[ CK_ORDER[Key_size / 10][1] ] );
		this->substitution( W[2], 0 );
		this->permutation( W[2] );
		this->x_or( W[2], W[0] );

		memcpy( W[3], W[2], Block_size );
		this->x_or( W[3], CK[ CK_ORDER[Key_size / 10][2] ] );
		this->substitution( W[3], 1 );
		this->permutation( W[3] );
		this->x_or( W[3], W[1] );

		#if ARIA_KEY_TEST // 测试输出 W[k]
		for ( size_t i = 0; i < 4; ++i )
		{
			for ( size_t j = 0; j < 16; ++j )
				printf("%02x ", W[i][j]);
			printf("\n");
		}
		printf("W[k] END\n\n");
		#endif
		
		unsigned char temp[4][Block_size];

		/* ------------------- 然后计算 encrypt_round_key_ ------------------- */	
		memset( encrypt_round_key_[0], 0, Block_size ); // 刻意不用的……
		memset( decrypt_round_key_[0], 0, Block_size ); // 刻意不用的……

		for ( size_t i = 1; i < NUM_OF_ROUND_KEY; ++i )
		{
			memcpy( temp, W, 4 * Block_size );
			Rotate_block<Block_size>( ROUND_KEY_OP_TABLE[i][0], temp[ ROUND_KEY_CHOOSE_TABLE[i][0] ], ROUND_KEY_MOVE_BITS_TABLE[i][0] );
			Rotate_block<Block_size>( ROUND_KEY_OP_TABLE[i][1], temp[ ROUND_KEY_CHOOSE_TABLE[i][1] ], ROUND_KEY_MOVE_BITS_TABLE[i][1] );
			this->x_or( temp[ ROUND_KEY_CHOOSE_TABLE[i][0] ], temp[ ROUND_KEY_CHOOSE_TABLE[i][1] ] );
			memcpy( encrypt_round_key_[i], temp[ ROUND_KEY_CHOOSE_TABLE[i][0] ], Block_size );
		}


		/* ------------------- 然后计算 decrypt_round_key_ ------------------- */	
		memcpy( decrypt_round_key_[1], encrypt_round_key_[Rounds + 1], Block_size );
		memcpy( decrypt_round_key_[Rounds + 1], encrypt_round_key_[1], Block_size );
		
		for ( size_t i = 2; i <= Rounds; ++i )
		{
			memcpy( decrypt_round_key_[i], encrypt_round_key_[Rounds + 2 - i], Block_size );
			this->permutation( decrypt_round_key_[i] );
		}

		if ( ENCRYPT == t )
		{
			for ( size_t i = 1; i < Rounds + 2; ++i )
				memcpy( round_key_[i], encrypt_round_key_[i], Block_size );

			#if ARIA_KEY_TEST // output encrypt round key
			for ( size_t i = 1; i < Rounds + 2; ++i )
				triod::Print_block<Block_size>(encrypt_round_key_[i]);
			printf("ENCRYPT ROUND KEY END\n\n");
			#endif

		}
		if ( DECRYPT == t )
		{
			for ( size_t i = 1; i < Rounds + 2; ++i )
				memcpy( round_key_[i], decrypt_round_key_[i], Block_size );

			#if ARIA_KEY_TEST // output encrypt round key
			for ( size_t i = 1; i < Rounds + 2; ++i )
				triod::Print_block<Block_size>(decrypt_round_key_[i]);
			printf("DECRYPT ROUND KEY END\n\n");
			#endif

		}

	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void ARIA_Key_scheduler<Block_size, Key_size, Rounds>::set_key( key_block key )
	{
		// very important for ARIA! must init with Zero!!!
		memset( key_, 0, sizeof(key_) );

		memcpy( key_ + MAX_KEY_WIDTH - Key_size, key, Key_size );
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void ARIA_Key_scheduler<Block_size, Key_size, Rounds>::fill_round_key( size_t nth_round, block round_key )
	{
		memcpy( round_key, round_key_[nth_round], Block_size );
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void ARIA_Key_scheduler<Block_size, Key_size, Rounds>::substitution ( block in, size_t round )
	{
		for ( size_t j = 0; j < Block_size; ++j )
		{
			in[j] = SBOX[ round % 2 == 1 ? ODD_LAYER_BOX[j] : EVEN_LAYER_BOX[j] ][ in[j] ];
		}
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void ARIA_Key_scheduler<Block_size, Key_size, Rounds>::permutation( block in ) 		// The Diffusion Layer Function
	{
		unsigned char temp[Block_size];
		memcpy ( temp, in, Block_size );

		for ( size_t i = 0; i < Block_size; ++i )
		{
			in[i] = 
				temp[ DIFFUSE_OPERATION[i][0] ] ^
				temp[ DIFFUSE_OPERATION[i][1] ] ^
				temp[ DIFFUSE_OPERATION[i][2] ] ^
				temp[ DIFFUSE_OPERATION[i][3] ] ^
				temp[ DIFFUSE_OPERATION[i][4] ] ^
				temp[ DIFFUSE_OPERATION[i][5] ] ^
				temp[ DIFFUSE_OPERATION[i][6] ];
		}
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void ARIA_Key_scheduler<Block_size, Key_size, Rounds>::x_or( block in, const_block x )
	{
		for ( size_t i = 0; i < Block_size; ++i )
			in[i] ^= x[i];
	}



} // End of Namespace

#endif

