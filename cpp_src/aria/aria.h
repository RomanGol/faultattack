/*

Filename:		ARIA.h
Function:		block cipher ARIA
Maintainer:		RomanGol  romangoliard@gmail.com
Last Modify:	2010-10
Comment:		due to template��cannot compiled seperately ����
				2009-10, dervied from SPN
				2010-10, fix bug

*/

#ifndef _ARIA_H_
#define _ARIA_H_

#include <cstdio>
#include <ctime>
#include <cstring>
#include <vector>

#include "spn.h"
#include "util_le.h"
#include "aria_const.h"
#include "aria_key.h"

#define IMPORT_ERROR 1

namespace triod
{
	// implementation starts here
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class ARIA : public SPN<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)	[Block_size];
		typedef unsigned char			(&block)		[Block_size];
		typedef const unsigned char		(&key_block)	[Key_size];

		ARIA();
		

		void encrypt( const_block plain, block cipher, key_block key );
		void decrypt( const_block cipher, block plain, key_block key );
		void set_key( key_block key );


	/* ---------------------------------------------------------------------------------------- */
	protected:
		void permutation	( block in );
		void substitution	( block in, size_t round );
		void key_addition	( block input, const_block key );

		void substitution	( block in );
		void key_schedule	( Op_type t, key_block key );

		//unsigned char key_[MAX_KEY_WIDTH];
		unsigned char encrypt_round_key_[NUM_OF_ROUND_KEY][Block_size];
		unsigned char decrypt_round_key_[NUM_OF_ROUND_KEY][Block_size];

		unsigned short	round_counter_;
		unsigned char	key_[Key_size];
		unsigned char	round_key_[Rounds + 1][Block_size];	
	};
}

/* -------------------------------------------------------------------------------------------------------------------------------- */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	ARIA<Block_size, Key_size, Rounds>::ARIA()
	{
		// Guard, used to forbid illegal Key_size
		char guard[(Key_size == MIN_LEN || Key_size == MID_LEN || Key_size == MAX_LEN) ? 1 : -1] = { 0 };

		// Guard, used to forbid illegal Rounds
		char guard_a[(Rounds == MIN_ROUND || Rounds == MID_ROUND || Rounds == MAX_ROUND) ? 1 : -1] = { 0 };
	}


	
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void ARIA<Block_size, Key_size, Rounds>::encrypt( const_block plain, block cipher, key_block key )	// 
	{
		this->key_schedule( ENCRYPT, key );
		memcpy ( cipher, plain, Block_size );
		/*======================================*/
		
		for ( size_t i = 1; i < Rounds; ++i )
		{
			this->key_addition( cipher, encrypt_round_key_[i] );
			this->substitution( cipher, i );
			this->permutation( cipher );

			#if ARIA_TEST
			Print_block<Block_size>( cipher );
			#endif
		}

		// Last round
		key_addition( cipher, encrypt_round_key_[Rounds] );
		for ( size_t i = 0; i < Block_size; ++i )		
			cipher[i] = SBOX[ EVEN_LAYER_BOX[i] ][ cipher[i] ];
		key_addition( cipher, encrypt_round_key_[Rounds + 1] );
		// Last round

		#if ARIA_TEST
		Print_block<Block_size>( cipher );
		printf( "Encrypt TEXT\n\n");
		#endif

		this->round_counter_ = 0;
	}

	
	// Notice that Rounds value is less than actual encryption rounds by one
	// because last round has no diffuse operation
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void ARIA<Block_size, Key_size, Rounds>::decrypt( const_block cipher, block plain, key_block key )	
	{
		key_schedule( DECRYPT, key );
		memcpy ( plain, cipher, Block_size );
		/*======================================*/

		for ( size_t i = 1; i < Rounds; ++i )
		{
			this->key_addition( plain, decrypt_round_key_[i] );
			this->substitution( plain, i );
			this->permutation( plain );

			#if ARIA_TEST
			Print_block<Block_size>( plain );
			#endif
		}

		// Last round
		this->key_addition( plain, decrypt_round_key_[Rounds] );
		for ( size_t i = 0; i < Block_size; ++i )
			plain[i] = SBOX[ EVEN_LAYER_BOX[i] ][ plain[i] ];
		key_addition( plain, decrypt_round_key_[Rounds + 1] );

		#if ARIA_TEST
		Print_block<Block_size>( plain );
		printf( "Decrypt TEXT\n\n");
		#endif
	}





/* ---------------------------------------------------------------------------------------- */
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void ARIA<Block_size, Key_size, Rounds>::substitution ( block in, size_t round )
	{
		for ( size_t j = 0; j < Block_size; ++j )
		{
			in[j] = SBOX[ round % 2 == 1 ? ODD_LAYER_BOX[j] : EVEN_LAYER_BOX[j] ][ in[j] ];
		}
	}


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void ARIA<Block_size, Key_size, Rounds>::substitution ( block in )
	{
		// dummy
	}



	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void ARIA<Block_size, Key_size, Rounds>::key_schedule ( Op_type t, key_block key )
	{
		ARIA_Key_scheduler<Block_size, Key_size, Rounds> ks;
		ks.set_key( key );
		ks.key_schedule( t );

		if ( ENCRYPT == t )
		{
			for ( size_t i = 1; i < Rounds + 2; ++i )
				ks.fill_round_key( i, encrypt_round_key_[i] );
		}
		if ( DECRYPT == t )
		{
			for ( size_t i = 1; i < Rounds + 2; ++i )
				ks.fill_round_key( i, decrypt_round_key_[i] );
		}
	}
	
	// The Diffusion Layer Function
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void ARIA<Block_size, Key_size, Rounds>::permutation( block in )
	{
		unsigned char temp[Block_size];
		memcpy ( temp, in, Block_size );

		for ( size_t i = 0; i < Block_size; ++i )
		{
			in[i] = 
				temp[ DIFFUSE_OPERATION[i][0] ] ^
				temp[ DIFFUSE_OPERATION[i][1] ] ^
				temp[ DIFFUSE_OPERATION[i][2] ] ^
				temp[ DIFFUSE_OPERATION[i][3] ] ^
				temp[ DIFFUSE_OPERATION[i][4] ] ^
				temp[ DIFFUSE_OPERATION[i][5] ] ^
				temp[ DIFFUSE_OPERATION[i][6] ];
		}
	}


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void ARIA<Block_size, Key_size, Rounds>::set_key( key_block key )
	{
		for ( size_t i = 0; i < Key_size; ++i )
			key_[i] = key[i];
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void ARIA<Block_size, Key_size, Rounds>::key_addition( block input, const_block key )
	{
		for ( size_t i = 0; i < Key_size; ++i )
			input[i] = input[i] ^ key[i];
	}


/* -------------------------------------------------------------------------------------------------------------------------------- */
}


#endif
