/*

Filename:		printcipher_fault.h
Function:		Block cipher PrintCipher fault attacker
Maintainer:		RomanGol  romangoliard@gmail.com
Last Modify:	2011-12
Comment:		

*/


#ifndef _PRINT_CIPHER_FAULT_H_
#define _PRINT_CIPHER_FAULT_H_

#include "printcipher.h"

/* ------------------------ Declaration	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class PRINT_CIPHER_FAULT : public PRINT_CIPHER<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)	[Block_size];
		typedef unsigned char			(&block)		[Block_size];
		typedef const unsigned char		(&key_block)	[Key_size];

		void fncrypt ( const_block plain, block cipher, key_block key, 
				const size_t e_round, const size_t e_pos, const uint8_t err );
		
		void multi_fncrypt ( const_block plain, block cipher, key_block key, 
				const size_t e_round, const size_t e_pos, const uint8_t err );
	};
} // End of Namespace


/* ------------------------ Implementation 	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRINT_CIPHER_FAULT<Block_size, Key_size, Rounds>::fncrypt( const_block plain, block cipher, key_block key, 
												const size_t e_round, const size_t e_pos, const uint8_t err )
	{
		unsigned char RC[Rounds];
		
		const static uint8_t bits_of_unit = 3;
		const static uint8_t bits_of_byte = 8;

		if ( 48 == Rounds )
			memcpy( RC, RC48, sizeof(RC) );
		else
			memcpy( RC, RC96, sizeof(RC) );

		memcpy( cipher, plain, Block_size );
		this->key_schedule( ENCRYPT, key );
		
		for ( this->round_counter_ = 0; this->round_counter_ < Rounds; ++this->round_counter_ )
		{
			if ( e_round == this->round_counter_ )
			{
				uint8_t v[Block_size * bits_of_byte];
				uint8_t u[Block_size * bits_of_byte / bits_of_unit];
				triod::To_bit_array<Block_size, sizeof(v)>( cipher, v );
				triod::Split_bit_array<sizeof(v), sizeof(u)>( v, u, bits_of_unit );
				/* import error */
				u[e_pos] ^= err; 			

				triod::To_bit_array<sizeof(u), sizeof(v)>( u, v, bits_of_unit );
				triod::Split_bit_array<sizeof(v), sizeof(cipher)>(v, cipher, bits_of_byte );
			}

			this->key_addition( cipher, this->round_key_[this->round_counter_] );
			this->permutation( cipher );
			cipher[0] ^= RC[this->round_counter_];
			this->substitution( cipher );
		}
		this->round_counter_ = 0;
	}
	
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRINT_CIPHER_FAULT<Block_size, Key_size, Rounds>::multi_fncrypt( const_block plain, block cipher, key_block key, 
												const size_t e_round, const size_t e_pos, const uint8_t err )
	{
		unsigned char RC[Rounds];

		if ( 48 == Rounds )
			memcpy( RC, RC48, sizeof(RC) );
		else
			memcpy( RC, RC96, sizeof(RC) );

		memcpy( cipher, plain, Block_size );
		this->key_schedule( ENCRYPT, key );
		
		for ( this->round_counter_ = 0; this->round_counter_ < Rounds; ++this->round_counter_ )
		{
			if ( e_round == this->round_counter_ )
			{
				const static uint8_t bits_of_unit = 3;
				const static uint8_t bits_of_byte = 8;

				uint8_t v[Block_size * bits_of_byte];
				uint8_t u[Block_size * bits_of_byte / bits_of_unit];
				triod::To_bit_array<Block_size, sizeof(v)>( cipher, v );
				triod::Split_bit_array<sizeof(v), sizeof(u)>( v, u, bits_of_unit );

				int m[3];
				m[0] = 1 + rand() % 15;
				m[1] = 1 + rand() % 15;
				m[2] = 1 + rand() % 15;
			
				while ( m[1] == m[0] || m[1] == m[2] || m[2] == m[0] )
				{
					m[1] =  1 + rand() % 15;
					m[2] =  1 + rand() % 15;
				}
				

				
				/* import error */
				u[e_pos] ^= err; 
				u[(e_pos + m[0]) % 16] ^= (rand() % 7 + 1);
				u[(e_pos + m[1]) % 16] ^= (rand() % 7 + 1);

				triod::To_bit_array<sizeof(u), sizeof(v)>( u, v, bits_of_unit );
				triod::Split_bit_array<sizeof(v), sizeof(cipher)>(v, cipher, bits_of_byte );				
			}

			this->key_addition( cipher, this->round_key_[this->round_counter_] );
			this->permutation( cipher );
			cipher[0] ^= RC[this->round_counter_];
			this->substitution( cipher );
		}
		this->round_counter_ = 0;
	}	
	

} // End of Namespace

#endif
