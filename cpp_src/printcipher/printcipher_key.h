/*

Filename:	printcipher_key.h
Function:		PrintCipher Key Scheduler
Maintainer:	RomanGol  romangoliard@gmail.com
Last Modify:	2010-08
Comment:		

*/

#ifndef _PRINT_CIPHER_KEYSCHEDR_H_
#define _PRINT_CIPHER_KEYSCHEDR_H_

#include "../util_le.h"
#include "../keyschedr_spn.h"

/* ------------------------ Declaration	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class PRINT_CIPHER_Key_scheduler : public Key_scheduler<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)	[Block_size];
		typedef unsigned char			(&block)		[Block_size];
		typedef const unsigned char		(&key_block)	[Key_size];

		void fill_round_key	( size_t nth_round, block round_key );
		void set_key		( key_block key );
		void key_schedule	( Op_type t );

	protected:
		unsigned char	key_[Key_size];
		unsigned char	round_key_[Rounds + 1][Block_size];		
	};


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRINT_CIPHER_Key_scheduler<Block_size, Key_size, Rounds>::key_schedule( Op_type t )
	{
		/* a very simple key_schedule */
		if ( ENCRYPT == t )
		{
			for ( size_t i = 0; i < Rounds; ++i )
			{
				memcpy( round_key_[i], key_, Block_size );
			}
			memcpy( round_key_[Rounds], key_, Block_size );
		}
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRINT_CIPHER_Key_scheduler<Block_size, Key_size, Rounds>::set_key( key_block key )
	{
		for ( size_t i = 0; i < Key_size; ++i )
			key_[i] = key[i];
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRINT_CIPHER_Key_scheduler<Block_size, Key_size, Rounds>::fill_round_key( size_t nth_round, block round_key )
	{
		memcpy( round_key, round_key_[nth_round], Block_size );
	}

} // End of Namespace




#endif
