#include <iostream>
#include <ctime>
#include <cstdlib>
#include "printcipher.h"
#include "printcipher_fault.h"
#include "../util_le.h"

static const unsigned char RC96[] = 
{
	0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7E, 0x7D, 
	0x7B, 0x77, 0x6F, 0x5F, 0x3E, 0x7C, 0x79, 0x73, 
	0x67, 0x4F, 0x1E, 0x3D, 0x7A, 0x75, 0x6B, 0x57, 
	0x2E, 0x5C, 0x38, 0x70, 0x61, 0x43, 0x06, 0x0D, 
	0x1B, 0x37, 0x6E, 0x5D, 0x3A, 0x74, 0x69, 0x53, 
	0x26, 0x4C, 0x18, 0x31, 0x62, 0x45, 0x0A, 0x15, 
	0x2B, 0x56, 0x2C, 0x58, 0x30, 0x60, 0x41, 0x02, 
	0x05, 0x0B, 0x17, 0x2F, 0x5E, 0x3C, 0x78, 0x71, 
	0x63, 0x47, 0x0E, 0x1D, 0x3B, 0x76, 0x6D, 0x5B, 
	0x36, 0x6C, 0x59, 0x32, 0x64, 0x49, 0x12, 0x25, 
	0x4A, 0x14, 0x29, 0x52, 0x24, 0x48, 0x10, 0x21, 
	0x42, 0x04, 0x09, 0x13, 0x27, 0x4E, 0x1C, 0x39
};

static const unsigned char RC48[] = 
{
	0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3E, 0x3D, 0x3B, 
	0x37, 0x2F, 0x1E, 0x3C, 0x39, 0x33, 0x27, 0x0E, 
	0x1D, 0x3A, 0x35, 0x2B, 0x16, 0x2C, 0x18, 0x30, 
	0x21, 0x02, 0x05, 0x0B, 0x17, 0x2E, 0x1C, 0x38, 
	0x31, 0x23, 0x06, 0x0D, 0x1B, 0x36, 0x2D, 0x1A, 
	0x34, 0x29, 0x12, 0x24, 0x08, 0x11, 0x22, 0x04
};

const static unsigned char Rbox[4][8] = 
{
	{0, 1, 7, 2, 5, 6, 3, 4},
	{0, 1, 7, 4, 3, 6, 5, 2},
	{0, 2, 7, 1, 6, 5, 3, 4},
	{0, 4, 7, 2, 5, 3, 6, 1}
};

const static uint8_t Rtable[] = 
{
	0, 16, 32, 1, 17, 33, 2, 18, 34, 3, 
	19, 35, 4, 20, 36, 5, 21, 37, 6, 22, 
	38, 7, 23, 39, 8, 24, 40, 9, 25, 41, 
	10, 26, 42, 11, 27, 43, 12, 28, 44, 
	13, 29, 45, 14, 30, 46, 15, 31, 47
};

static unsigned char Stable[6 * 8 / 3];
const static uint8_t bits_of_unit = 3;
const static uint8_t bits_of_byte = 8;

void reverse( uint8_t (&in)[6], uint8_t (&key)[10], uint8_t round )	
{
	uint8_t v[sizeof(in) * bits_of_byte];
	uint8_t u[sizeof(in) * bits_of_byte / bits_of_unit];
	triod::To_bit_array<sizeof(in), sizeof(v)>( in, v );
	triod::Split_bit_array<sizeof(v), sizeof(u)>( v, u, bits_of_unit );

	for ( size_t i = 0; i < sizeof(u); ++i )
	{
		u[i] = Rbox[ Stable[i] ][ u[i] ];
	}
	triod::To_bit_array<sizeof(u), sizeof(v)>( u, v, bits_of_unit );
	triod::Split_bit_array<sizeof(v), sizeof(in)>(v, in, bits_of_byte );

	in[0] ^= RC48[round];
	triod::Bit_permutation<sizeof(in)>( in, in, Rtable );

	for ( size_t i = 0; i < sizeof(in); ++i )
	{
		in[i] ^= key[i];
	}
}

int printcipher_test()
{
	unsigned char text_g[] = { 0xC2, 0x00, 0x00, 0x00, 0x00, 0x00 };
	unsigned char text_f[] = { 0x42, 0x00, 0x00, 0x00, 0x00, 0x00 };

	unsigned char cipher96[] =	{ 0x45, 0x49, 0x6A, 0x12, 0x83, 0xEF, 0x56, 0xAF, 0xBD, 0xDC, 0x88, 0x81 };
	unsigned char text1[] =		{ 0xE1, 0xD1, 0xC2, 0xCD, 0x50, 0x7A, 0x83, 0xA9, 0x95, 0xE8, 0x97, 0x5A };
	unsigned char key1[] =		{ 0x46, 0x08, 0x94, 0xF6, 0x8F, 0x64, 0xBF, 0xA9, 0xBF, 0xDB, 0x3D, 0x95, 
									0x68, 0x67, 0x35, 0x90, 0xF0, 0x2A, 0xF2, 0x70, };

	unsigned char text2[] = { 0x34, 0x78, 0xCD, 0x86, 0x62, 0xAA, 0x9D, 0xB4, 0x96, 0xB3, 0x3B, 0xA8 };
	unsigned char key2[] = { 0x10, 0x45, 0xA1, 0x1A, 0x13, 0xE8, 0x84, 0x10, 0xEF, 0x1C, 0x3F, 0xD8,
								0xD0, 0x8D, 0x55, 0x0D, 0x89, 0x7A, 0xC6, 0x62 }; 
	
	unsigned char text3[] = { 0x4B, 0x0B, 0x1B, 0x35, 0xAC, 0xC3, 0xF3, 0x16, 0x58, 0x2A, 0xED, 0x5C };
	unsigned char key3[] = { 0x8A, 0x0B, 0xF5, 0x3E, 0xCF, 0x42, 0x04, 0x02, 0xEF, 0xCF, 0x5E, 0xEC, 
								0xE5, 0xEF, 0xA0, 0xEB, 0x6C, 0x81, 0xEA, 0x68 };
	

	unsigned char text4[] = { 0xCC, 0xA3, 0x0C, 0x69, 0x42, 0x98, 0x49, 0x74, 0x43, 0x27, 0xD7, 0x61 };
	unsigned char key4[] = { 0x73, 0xB1, 0xF0, 0xBA, 0xB5, 0xB4, 0xE6, 0x9E, 0x7A, 0x64, 0x3F, 0x2F, 
								0x24, 0x8D, 0xB4, 0x02, 0x69, 0xF3, 0x7C, 0xA0 };
	
	triod::PRINT_CIPHER<sizeof(text1), sizeof(key1), 96> liarod;
	liarod.encrypt( text1, cipher96, key1 );	
	triod::Print_block<sizeof(cipher96)>( cipher96 );	// 45496A1283EF56AFBDDC8881
	liarod.encrypt( text2, cipher96, key2 );
	triod::Print_block<sizeof(cipher96)>( cipher96 );	//EE5A079934D98684DE165AC0
	liarod.encrypt( text3, cipher96, key3 );
	triod::Print_block<sizeof(cipher96)>( cipher96 );	//7F49205AF958DD440ED35D9E
	liarod.encrypt( text4, cipher96, key4 );
	triod::Print_block<sizeof(cipher96)>( cipher96 );	// 3EB4830D385EA369C1C82129

	return 0;
}

void printcipher_attack()
{
	uint8_t text[] = { 0x5B, 0xC3, 0x55, 0x75, 0x84, 0x4C };
	uint8_t key[] = { 0x7B, 0x32, 0xBA, 0x95, 0x88, 0xC2, 0xB6, 0xCD, 0xD2, 0x69 };
	//uint8_t key[] = { 0x7B, 0x32, 0xBA, 0x95, 0x88, 0xC2, 0x00, 0x00, 0x00, 0x00 };
	uint8_t cipher[] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	uint8_t fault[] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	
	triod::PRINT_CIPHER_FAULT<sizeof(text), sizeof(key), 48> roman;
	/*
	roman.encrypt( text, cipher, key );
	triod::Print_block<sizeof(cipher)>( cipher ); // EB 4A F9 5E 7D 37
	*/

	const static size_t err_round = 41;
	const static size_t num_of_pairs = 0x10;
	static uint8_t result[2][num_of_pairs][6];


	/* from Block_size are perm_key */
	for ( size_t i = sizeof(text); i < sizeof(key); ++i )
	{
		for( size_t j = 0; j < 4; ++j )
		{
			Stable[(i - sizeof(text)) * 4 + j] = static_cast<unsigned char>
			( 
				 ( key[i] >> (j * 2) ) 
				 & 
				 0x03
			);
		}
	}
	
	for ( uint32_t first = 0x0; first < 0x10; ++first )
	{
		for ( uint8_t second = 0x0; second < 0x10; ++second )
		{
			// unsigned char ultimate_key[8];

			uint32_t stat[16];
			memset( stat, 0, sizeof(stat) );
			for ( size_t j = 0; j < num_of_pairs; ++j )
			{
				roman.encrypt( text, cipher, key );	
				roman.fncrypt( text, fault, key, err_round, rand() % 16, (cipher[1] % 7 + 1)  );
				//roman.multi_fncrypt( text, fault, key, err_round, cipher[0] % 16, (cipher[1] % 7 + 1)  );

				reverse( cipher, key, 47 );
				reverse( fault, key, 47 );

				reverse( cipher, key, 46 );
				reverse( fault, key, 46 );

				// calc diff cipher text
				triod::X_or<sizeof(cipher)>( cipher, fault );

				++stat[ cipher[5] >> 5 ];

				// update plain text
				memcpy( text, cipher, sizeof(text) );
			}
		}
	}
}

void printcipher_sim()
{
	uint8_t text[] = { 0x5B, 0xC3, 0x55, 0x75, 0x84, 0x4C };
	uint8_t key[] = { 0x7B, 0x32, 0xBA, 0x95, 0x88, 0xC2, 0xB6, 0xCD, 0xD2, 0x69 };
	uint8_t cipher[] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	uint8_t fault[] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	
	triod::PRINT_CIPHER_FAULT<sizeof(text), sizeof(key), 48> roman;

	uint8_t units[16];
	uint8_t bits[48];
	uint32_t err_round;
	printf( "Input faulty round: \n" );
	scanf( "%d", &err_round );
	const static size_t num = 10000;

	uint32_t stat[16][8]; // 16 tri-bit, each has 2^3 = 8 candidates
	memset( stat, 0, sizeof(stat) );
	
	for ( size_t j = 0; j < num; ++j )
	{
		roman.encrypt( text, cipher, key );
		roman.fncrypt( text, fault, key, err_round, rand() % 16, (rand() % 7 + 1)  );
		//roman.multi_fncrypt( text, fault, key, err_round, cipher[0] % 16, (cipher[1] % 7 + 1)  );

		triod::X_or<sizeof(cipher)>( cipher, fault ); // calc diff cipher text

		triod::To_bit_array<6, 48>(cipher, bits);
		triod::Split_bit_array<48, 16>(bits, units, 3); // get each tri-bit 
		
		for ( size_t k = 0; k < sizeof(units); ++k )
		{
			++stat[k][units[k] & 0x7];
		}
		
		// update plain text
		memcpy( text, fault, sizeof(fault) );
	}

	for ( size_t k = 0; k < sizeof(units); ++k )
	{
		double squared_euclid = 0.0;
		for (size_t i = 0; i < 8; ++i)
			squared_euclid += ( 1.0 / 8 - stat[k][i] * 1.0 / num ) * ( 1.0 / 8 - stat[k][i] * 1.0 / num );
		printf( "%0.4f\n", squared_euclid );
	}

}

int attack()
{

	printf("----PRINT CIPHER----\n");

#define print_sim 1

#ifdef print_test
	printcipher_test();
#endif

#ifdef print_attack
	printcipher_attack();
#endif

#ifdef print_sim
	printcipher_sim();
#endif
	return 0;
}

