#ifndef _PRINT_CIPHER_H_
#define _PRINT_CIPHER_H_

#include "../spn.h"
#include "../util_le.h"
#include "printcipher_key.h"

// const static unsigned short ERROR_PLACE = 3;

/* ------------------------ Declaration	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class PRINT_CIPHER : public SPN<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)	[Block_size];
		typedef unsigned char			(&block)		[Block_size];
		typedef const unsigned char		(&key_block)	[Key_size];

		PRINT_CIPHER();
		void encrypt	( const_block plain, block cipher, key_block key );
		void decrypt	( const_block cipher, block plain, key_block key );
		
	protected:
		void key_addition	( block input, const_block key );
		void permutation	( block in );
		void substitution	( block in );
		void key_schedule	( Op_type t, key_block key );

		unsigned char Ptable_[Block_size * 8];
		unsigned char Stable_[Block_size * 8 / 3];

		unsigned short	round_counter_;
		unsigned char	key_[Key_size];
		unsigned char	round_key_[Rounds + 1][Block_size];	
	};
} // End of Namespace


/* ------------------------ Implementation 	------------------------ */
namespace triod
{
	static const unsigned char RC96[] = 
	{
		0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7E, 0x7D, 
		0x7B, 0x77, 0x6F, 0x5F, 0x3E, 0x7C, 0x79, 0x73, 
		0x67, 0x4F, 0x1E, 0x3D, 0x7A, 0x75, 0x6B, 0x57, 
		0x2E, 0x5C, 0x38, 0x70, 0x61, 0x43, 0x06, 0x0D, 
		0x1B, 0x37, 0x6E, 0x5D, 0x3A, 0x74, 0x69, 0x53, 
		0x26, 0x4C, 0x18, 0x31, 0x62, 0x45, 0x0A, 0x15, 
		0x2B, 0x56, 0x2C, 0x58, 0x30, 0x60, 0x41, 0x02, 
		0x05, 0x0B, 0x17, 0x2F, 0x5E, 0x3C, 0x78, 0x71, 
		0x63, 0x47, 0x0E, 0x1D, 0x3B, 0x76, 0x6D, 0x5B, 
		0x36, 0x6C, 0x59, 0x32, 0x64, 0x49, 0x12, 0x25, 
		0x4A, 0x14, 0x29, 0x52, 0x24, 0x48, 0x10, 0x21, 
		0x42, 0x04, 0x09, 0x13, 0x27, 0x4E, 0x1C, 0x39
	};

	static const unsigned char RC48[] = 
	{
		0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3E, 0x3D, 0x3B, 
		0x37, 0x2F, 0x1E, 0x3C, 0x39, 0x33, 0x27, 0x0E, 
		0x1D, 0x3A, 0x35, 0x2B, 0x16, 0x2C, 0x18, 0x30, 
		0x21, 0x02, 0x05, 0x0B, 0x17, 0x2E, 0x1C, 0x38, 
		0x31, 0x23, 0x06, 0x0D, 0x1B, 0x36, 0x2D, 0x1A, 
		0x34, 0x29, 0x12, 0x24, 0x08, 0x11, 0x22, 0x04
	};

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	PRINT_CIPHER<Block_size, Key_size, Rounds>::PRINT_CIPHER()
	{
		/* init the diffusion table */
		for ( size_t i = 0; i < Block_size * 8 - 1; ++i )
			Ptable_[i] = static_cast<unsigned char>( 3 * i % (Block_size * 8 - 1) );
		Ptable_[Block_size * 8 - 1] = Block_size * 8 - 1;
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRINT_CIPHER<Block_size, Key_size, Rounds>::encrypt( const_block plain, block cipher, key_block key )
	{
		unsigned char RC[Rounds];

		if ( 48 == Rounds )
			memcpy( RC, RC48, sizeof(RC) );
		else
			memcpy( RC, RC96, sizeof(RC) );

		memcpy( cipher, plain, Block_size );
		this->key_schedule( ENCRYPT, key );
		
		for ( round_counter_ = 0; round_counter_ < Rounds; ++round_counter_ )
		{
			this->key_addition( cipher, round_key_[round_counter_] );
			this->permutation( cipher );
			cipher[0] ^= RC[round_counter_];
			this->substitution( cipher );
		}
		this->round_counter_ = 0;
 	}
	


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRINT_CIPHER<Block_size, Key_size, Rounds>::decrypt( const_block cipher, block plain, key_block key )
	{
		// no implementation
	}	
	

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRINT_CIPHER<Block_size, Key_size, Rounds>::key_addition ( block input, const_block key )
	{
		for ( size_t i = 0; i < Block_size; ++i )
			input[i] ^= key[i];
	}

	
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRINT_CIPHER<Block_size, Key_size, Rounds>::permutation( block in )
	{
		triod::Bit_permutation<Block_size>( in, in, Ptable_ );
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRINT_CIPHER<Block_size, Key_size, Rounds>::substitution( block in )
	{
		const static uint8_t bits_of_unit = 3;
		const static uint8_t bits_of_byte = 8;
		const static unsigned char Sbox[4][8] = 
		{
			{0, 1, 3, 6, 7, 4, 5, 2},
			{0, 1, 7, 4, 3, 6, 5, 2},
			{0, 3, 1, 6, 7, 5, 4, 2},
			{0, 7, 3, 5, 1, 4, 6, 2}
		};

		uint8_t v[Block_size * bits_of_byte];
		uint8_t u[Block_size * bits_of_byte / bits_of_unit];
		triod::To_bit_array<Block_size, sizeof(v)>( in, v );
		triod::Split_bit_array<sizeof(v), sizeof(u)>( v, u, bits_of_unit );

	
		for ( size_t i = 0; i < sizeof(u); ++i )
		{
			u[i] = Sbox[ Stable_[i] ][ u[i] ];
		}
		triod::To_bit_array<sizeof(u), sizeof(v)>( u, v, bits_of_unit );
		triod::Split_bit_array<sizeof(v), sizeof(in)>(v, in, bits_of_byte );
	}

	
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRINT_CIPHER<Block_size, Key_size, Rounds>::key_schedule( Op_type t, key_block key )
	{
		PRINT_CIPHER_Key_scheduler<Block_size, Key_size, Rounds> ks;
		ks.set_key( key );
		ks.key_schedule( t );

		for ( size_t i = 0; i < Rounds + 1; ++i )
		{
			ks.fill_round_key( i, round_key_[i] );
		}

		/* from Block_size are perm_key */
		for ( size_t i = Block_size; i < Key_size; ++i )
		{
			for( size_t j = 0; j < 4; ++j )
			{
				Stable_[(i - Block_size) * 4 + j] = static_cast<unsigned char>
													( 
														( key[i] >> (j * 2) ) 
															& 
														0x03
													);
			}
		}
	}

} // End of Namespace

#endif
