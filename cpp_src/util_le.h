/*

Filename:		util_le.h
Function:		some utilities to do crypto operations
Maintainer:		RomanGol  romangoliard@gmail.com
Last Modify:	2011-11
Comment:		due to template，cannot compiled seperately ……
				Little Endian

*/

#ifndef _UTIL_LE_H_
#define _UTIL_LE_H_

#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "dfa.h"

namespace triod
{
	const static unsigned short Bits_of_byte = 8;
	
	template<size_t Length>
	extern bool Compare_section ( const uint8_t (&lhs)[Length], const uint8_t (&rhs)[Length] );

	template<size_t Length>
	extern void X_or ( uint8_t (&lhs)[Length], const uint8_t (&rhs)[Length] );
	
	template<size_t Length, size_t Tab_len>
	extern void Bit_permutation ( const uint8_t (&input)[Length], uint8_t (&output)[Length], const uint8_t (&ptable)[Tab_len], 
							const uint8_t bits_of_unit = Bits_of_byte );
	/*
	template<size_t Block_size>
	extern void Rotate_key ( Rotate_type types, uint8_t (&input)[Block_size], size_t move_bits );
	*/

	template<size_t Block_size>
	extern void Print_block( const uint8_t (&text)[Block_size] );

	template<size_t Block_size>
	extern void Rotate_block ( Rotate_type types, uint8_t (&input)[Block_size], size_t move_bits, const uint8_t bits_of_unit = Bits_of_byte );

	template<size_t Length>
	extern std::vector<uint8_t> To_bit_vector ( const uint8_t (&input)[Length], const uint8_t bits_of_unit = Bits_of_byte );
	
	extern std::vector<uint8_t> To_bit_vector( const std::vector<uint32_t> &v, const uint8_t bits_of_unit = Bits_of_byte );

	extern std::vector<uint32_t> Split_bit_vector( const std::vector<uint8_t> &v, const uint8_t bits_of_unit = Bits_of_byte );

	template<size_t Len_in, size_t Len_out>
	void Split_bit_array( const uint8_t (&input)[Len_in], uint8_t (&ouput)[Len_out], const uint8_t bits_of_unit = Bits_of_byte );
	
	template<size_t Len_in, size_t Len_out>
	void To_bit_array ( const uint8_t (&input)[Len_in], uint8_t (&output)[Len_out], const uint8_t bits_of_unit = Bits_of_byte );
	
	template<size_t Block_size>
	extern void Split_and_print_block( const uint8_t (&input)[Block_size], const uint8_t bits_of_unit = Bits_of_byte );
} // End of Namespace

namespace triod
{
	/**************************************************************
	Template:		template<size_t Block_size>
	Func name:		void Rotate_block ( Rotate_type types, uint8_t (&input)[Block_size], size_t move_bits )
	Usage:			used to rotate move bits of a block, Little Endian, 2011-11-21@RomanGol
	Param:			types
	Return:			void, return by input
	**************************************************************/
	template<size_t Block_size>
	void Rotate_block ( Rotate_type types, uint8_t (&input)[Block_size], size_t move_bits, const uint8_t bits_of_unit )
	{
		if ( NOP == types ) // add by RomanGol 2010-09-20
			return;

		uint8_t table[Block_size * Bits_of_byte];
		uint8_t output [Block_size];
		size_t old_pos;
		size_t new_pos;

		for ( size_t i = 0; i < Block_size; ++i )
		{
			for ( size_t j = 0; j < bits_of_unit; ++j )
			{
				old_pos = i * bits_of_unit + j;
				uint8_t c = static_cast<uint8_t>( (input[i] >> j) & 0x01 );
				
				if ( LEFT == types )
				{
					new_pos = (old_pos + move_bits) % sizeof(table);
					table[new_pos] = c;
				}
				if ( RIGHT == types )
				{
					new_pos = (sizeof(table) + old_pos - move_bits) % sizeof(table);
					table[new_pos] = c;
				}
			}
		}

		for ( size_t i = 0; i < Block_size; ++i )
		{
			output[i] = 0;
			for( size_t j = 0; j < bits_of_unit; ++j )
			{
				output[i] |= (table[i * bits_of_unit + j] << j);
			}
		}

		memcpy( input, output, Block_size );
	}


/*
	template<size_t Block_size>
	void Rotate_key ( Rotate_type types, uint8_t (&input)[Block_size], size_t move_bits )
	{
		uint8_t output [Block_size];
		size_t move_bytes = move_bits / 8;
		size_t remain_bits = move_bits % 8;
		
		uint8_t higher_bit;
		uint8_t lower_bit;

		if ( LEFT == types )
		{
			memcpy ( output, input + move_bytes, Block_size - move_bytes );
			memcpy ( output + Block_size - move_bytes, input, move_bytes );

			for ( size_t i = 0; i < remain_bits; ++i )
			{
				higher_bit = (output[0] & 0x80) >> 7;	// 保存最高位
				for ( size_t j = 0; j < Block_size - 1; ++j )
				{
					output[j] <<= 1;							// 左移之后，最低位用 0 补齐
					lower_bit = (output[j + 1] & 0x80) >> 7;
					output[j] |= lower_bit;						// 0 | lower_bit = lower_bit
				}

				output[Block_size - 1] <<= 1;
				output[Block_size - 1] |= higher_bit;
			}
		}

		if ( RIGHT == types )
		{
			memcpy ( output, input + Block_size - move_bytes, move_bytes );
			memcpy ( output + move_bytes, input, Block_size - move_bytes );

			for ( size_t i = 0; i < remain_bits; ++i )
			{
				lower_bit = (output[0] & 0x01) << 7;
				higher_bit = (output[Block_size - 1] & 0x01) << 7;	// 保存最低位
				output[0] >>= 1;														// 右移之后，最高位用 0 补齐
				output[0] |= higher_bit;											// 0 | lower_bit = lower_bit

				for ( size_t j = 1; j < Block_size; ++j )
				{
					higher_bit = lower_bit;
					lower_bit = (output[j] & 0x01) << 7;
					output[j] >>= 1;								// 右移之后，最高位用 0 补齐
					output[j] |= higher_bit;					// 0 | higher_bit = higher_bit
				}
			}
		}

		if ( NOP == types )
		{
			// do nothing;
		}

		memcpy ( input, output, Block_size );
	}
*/

	/**************************************************************
	Template:		template<size_t Block_size>
	Func name:	void Print_block( const uint8_t (&text)[Block_size] )
	Usage:	
	Param:	text
	Return:	void
	**************************************************************/
	template<size_t Block_size>
	void Print_block( const uint8_t (&text)[Block_size] )
	{
		for ( size_t i = 0; i < sizeof(text); ++i )
			printf( "0x%02X, ", text[i] );
		printf( "\n" );
	}


	/**************************************************************
	Template:		template<size_t Length>，Length表示输入block的字节长度
	Func name:	bool Compare_section( const uint8_t (&lhs)[Length], const uint8_t (&rhs)[Length] )
	Usage:	
	Param:	
	Return:	
	**************************************************************/
	template<size_t Length>
	bool Compare_section( const uint8_t (&lhs)[Length], const uint8_t (&rhs)[Length] )
	{
		for ( size_t i = 0; i < Length; ++i )
		{
			if ( lhs[i] != rhs[i] )
				return false;
		}

		return true;
	}


	/**************************************************************
	Template:		template<size_t Length>, Length referes to the size of input array.
	Func name:		void X_or( const uint8_t (&lhs)[Length], const uint8_t (&rhs)[Length] )
	Usage:			used to exclusive lhs with rhs uint8_t array, lhs and rhs must have same size	
	Param:			lhs, rhs are uint8_t array with same size. rhs is const.
	Return:			no return, return by lhs.	
	**************************************************************/
	template<size_t Length>
	void X_or( uint8_t (&lhs)[Length], const uint8_t (&rhs)[Length] )
	{
		for ( size_t i = 0; i < Length; ++i )
		{
			lhs[i] ^= rhs[i];
		}
	}


	/**************************************************************
	Template:		template<size_t Length>
	Func name:		void bit_permutation ( const uint8_t (&input)[Length], uint8_t (&output)[Length], const uint8_t (&ptable)[Length * 8] )
	Usage:	
					Little Endian
	Param:
	Return:	
	**************************************************************/
	template<size_t Length, size_t Tab_len>
	void Bit_permutation ( const uint8_t (&input)[Length], uint8_t (&output)[Length], const uint8_t (&ptable)[Tab_len], 
							const uint8_t bits_of_unit )
	{
		uint8_t table[Tab_len];
		uint8_t b;
		uint16_t pos;

		for ( size_t i = 0; i < Length; ++i )
		{
			for( size_t j = 0; j < bits_of_unit; ++j )
			{
				b = static_cast<uint8_t>( (input[i] >> j) & 0x01 );
				pos = i * bits_of_unit + j;
				// Little Endian 2011-11-21@RomanGol
				table[ ptable[pos] ] = b;
			}
		}

		for ( size_t i = 0; i < Length; ++i )
		{
			output[i] = 0;
			for( size_t j = 0; j < bits_of_unit; ++j )
			{
				pos = i * bits_of_unit + j;
				output[i] |= (table[pos] << j);
			}
		}
	}


	/**************************************************************
	Template:	template<size_t Length>
	Func name:	void To_bit_array ( const uint8_t (&input)[Length], uint8_t (&output)[Length * 8] )
	Usage:	
	Param:	
	Return:	
	**************************************************************/
	template<size_t Len_in, size_t Len_out>
	void To_bit_array ( const uint8_t (&input)[Len_in], uint8_t (&output)[Len_out], const uint8_t bits_of_unit )
	{
		for ( size_t i = 0; i < Len_in; ++i )
		{
			for( size_t j = 0; j < bits_of_unit; ++j )
			{
				output[i * bits_of_unit + j] = (	(input[i] >> j) & 0x01		);
			}
		}
	}

	/**************************************************************
	Template:		template<size_t Length>，Length表示输入block的字节长度
	Func name:	std::vector<uint8_t> To_bit_vector ( const uint8_t (&input)[Length] )
	Usage:	
	Param:	
	Return:	vector<uint8_t>
	**************************************************************/
	template<size_t Length>
	std::vector<uint8_t> To_bit_vector ( const uint8_t (&input)[Length], const uint8_t bits_of_unit )
	{
		std::vector<uint8_t> table(Length * bits_of_unit);

		for ( size_t i = 0; i < Length; ++i )
		{
			for( size_t j = 0; j < bits_of_unit; ++j )
			{
				table[i * bits_of_unit + j] = (	(input[i] >> j)	& 0x01		);
			}
		}
		return table;
	}

	/**************************************************************
	Func name:	std::vector<uint32_t> To_bit_vector( const std::vector<uint32_t> &v, int step )
	Usage:	
		
	
	Param:	
	Return:	vector<uint8_t>
	**************************************************************/
	std::vector<uint8_t> To_bit_vector( const std::vector<uint32_t> &v, const uint8_t bits_of_unit )
	{
		std::vector<uint8_t> table( v.size() * bits_of_unit );

		for ( size_t i = 0; i < v.size(); ++i )
		{
			for( size_t j = 0; j < bits_of_unit; ++j )
			{
				table[i * bits_of_unit + j] = (		(v[i] >> j)	& 0x01		);
			}
		}
		return table;
	}


	/**************************************************************
	Func name:	template<size_t step> std::vector<uint32_t> Split_bit_vector( const std::vector<uint32_t> &v, int step )
	Usage:		
	Param:	
	Return:	vector<uint32_t>
	**************************************************************/
	std::vector<uint32_t> Split_bit_vector( const std::vector<uint8_t> &v, const uint8_t bits_of_unit )
	{
		//static const int power[] = { 0, 1, 2, 4, 8, 16, 32, 64, 128 };
		//static const int power[] = { 1, 2, 4, 8, 16, 32, 64, 128 };
		
		uint32_t t = 0;
		std::vector<uint32_t> vec;
		for ( size_t i = 0; i < v.size(); ++i )
		{
			t += ( v[i] << (i % bits_of_unit) );

			if ( i % bits_of_unit == bits_of_unit - 1 )
			{
				vec.push_back(t);
				t = 0;
			}
		}
		return vec;
	}

	/**************************************************************
	Template:	template<size_t Length, size_t step>
	Func name:	void Split_bit_array( const uint8_t (&input)[Length], uint8_t (&ouput)[Length / step] )
	Usage:		
	Param:	
	Return:		void, return by output	
	**************************************************************/
	template<size_t Len_in, size_t Len_out>
	void Split_bit_array( const uint8_t (&input)[Len_in], uint8_t (&ouput)[Len_out], const uint8_t bits_of_unit )
	{
		uint32_t t = 0;
		size_t counter = 0;
		
		for ( size_t i = 0; i < Len_in; ++i )
		{
			t += ( input[i] << (i % bits_of_unit) );

			if ( i % bits_of_unit == bits_of_unit - 1 )
			{
				ouput[counter++] = t;
				t = 0;
			}
		}
	}
	
	/**************************************************************
	Func name:	std::vector<uint32_t> Split_and_print_block( const uint8_t (&input)[Block_size], size_t step )
	Usage:		give an array and a step value, the function will first split the array accodring to step value and 
				print the splitted array.
				Default operation mode is little endian
				For instance, if a array of bit is  111000110001, and the step is 3, 
				then we have 111,000,110,001, that is 7,0,3,6
	Param:		input is an array of uint8_t, step referes to the width of block wanna be splited
	Return:		no return, print to std_out	
	**************************************************************/
	template<size_t Block_size>
	void Split_and_print_block( const uint8_t (&input)[Block_size], const uint8_t bits_of_unit )
	{
		std::vector<uint32_t> v = Split_bit_vector( triod::To_bit_vector<Block_size>(input), bits_of_unit );
		for ( size_t i = 0; i < v.size(); ++i )
			printf( "0x%02x, ", v[i] );
		printf("\n");
	}
}

#endif
