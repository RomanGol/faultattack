
#ifndef _PUFFIN_H_
#define _PUFFIN_H_

#include <string.h>
#include "spn.h"
#include "util_le.h"


/* ------------------------ Declaration	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class PUFFIN : SPN<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)	[Block_size];
		typedef unsigned char			(&block)		[Block_size];
		typedef const unsigned char		(&key_block)	[Key_size];

		void encrypt	( const_block plain, block cipher, key_block key );
		void decrypt	( const_block cipher, block plain, key_block key );

		void encrypt_f	( const_block plain, block subkey, key_block key );

		void set_key	( key_block key );

	private:
		void key_addition	( block input, const_block key );
		void permutation	( block in );
		void substitution	( block in );
		void key_schedule	( Op_type t, key_block key );
		void subkey_select	( key_block key, block sub_key, const unsigned char (&table)[Block_size * 8] );

		unsigned short	round_counter_;
		unsigned char	key_[Key_size];
		unsigned char	round_key_[Rounds + 1][Block_size];	
	};
} // End of Namespace


/* ------------------------ Implementation 	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PUFFIN<Block_size, Key_size, Rounds>::encrypt( const_block plain, block cipher, key_block key )
	{
		this->set_key( key );
		memcpy( cipher, plain, Block_size );
		key_schedule( ENCRYPT, key );
		
		for ( this->round_counter_ = 0; this->round_counter_ < Rounds; ++this->round_counter_ )
		{
			key_addition( cipher, this->round_key_[this->round_counter_] );
			this->permutation( cipher );
			this->substitution( cipher );
		}

		key_addition( cipher, this->round_key_[Rounds] );
		this->permutation( cipher );
	}
	

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PUFFIN<Block_size, Key_size, Rounds>::encrypt_f( const_block plain, block cipher, key_block key )
	{
		this->set_key( key );
		memcpy( cipher, plain, Block_size );
		key_schedule( ENCRYPT, key );
		
		for ( this->round_counter_ = 0; this->round_counter_ < Rounds; ++this->round_counter_ )
		{
			key_addition( cipher, this->round_key_[this->round_counter_] );
			this->permutation( cipher );

			if ( Rounds - 2 == round_counter_ )
			{
				cipher[rand() % Block_size] ^= ( rand() % 0x0F + 1 );
			}

			this->substitution( cipher );
		}

		key_addition( cipher, this->round_key_[Rounds] );
		this->permutation( cipher );
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PUFFIN<Block_size, Key_size, Rounds>::decrypt( const_block cipher, block plain, key_block key )
	{
		// no implementation
	}	
	

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PUFFIN<Block_size, Key_size, Rounds>::set_key( key_block key )
	{
		for ( size_t i = 0; i < Key_size; ++i )
			key_[i] = key[i];
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PUFFIN<Block_size, Key_size, Rounds>::key_addition ( block input, const_block key )
	{
		for ( size_t i = 0; i < Block_size; ++i )
			input[i] ^= key[i];
	}


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PUFFIN<Block_size, Key_size, Rounds>::key_schedule( Op_type t, key_block key )
	{
		const static unsigned char Select_table[] = 
		{
			0x02, 0x7a, 0xe, 0x39, 0x58, 0x23, 0x61, 0x33, 0x38, 0x3e, 0x63, 0x45, 0x2d, 0x46, 0x5d, 0x32, 
			0x52, 0xd, 0x3, 0x15, 0x1f, 0x71, 0x53, 0x64, 0xb, 0x16, 0x1e, 0x40, 0x28, 0x5f, 0x77, 0x31, 
			0x2c, 0x35, 0x6f, 0x79, 0x1c, 0x50, 0x1d, 0x78, 0x60, 0x36, 0x19, 0x3f, 0x17, 0x74, 0x12, 0x8, 
			0x6e, 0x11, 0x2b, 0x55, 0xf, 0x5e, 0x29, 0x47, 0x1, 0x5a, 0x75, 0x7b, 0x25, 0x2f, 0x2a, 0x26
		};

		const static unsigned char Key_Ptable[] = 
		{
			0x15, 0x78, 0x7d, 0x6d, 0x4e, 0x50, 0x73, 0x36, 0x70, 0x14, 0x1c, 0x13, 0x37, 0x4b, 0x28, 0x6f, 
			0x2c, 0x6c, 0x5e, 0x56, 0x5d, 0x2b, 0x43, 0x7, 0x72, 0x44, 0x5, 0x4a, 0x52, 0x04, 0x35, 0x45, 
			0x16, 0x3c, 0x69, 0x66, 0x54, 0x7b, 0x6e, 0x33, 0x76, 0x1f, 0x63, 0x10, 0xe, 0x21, 0x7f, 0x5a, 
			0x39, 0x62, 0x77, 0x42, 0x1e, 0x61, 0x34, 0x46, 0x5b, 0x18, 0x25, 0x5c, 0x40, 0x1, 0x24, 0x1b, 
			0x17, 0x51, 0x57, 0xd, 0x5f, 0x75, 0x0, 0x8, 0x7c, 0x1a, 0x7e, 0x11, 0x3, 0x9, 0x65, 0x06, 
			0x22, 0x68, 0x2f, 0x3e, 0x1d, 0x4c, 0x47, 0x31, 0x6b, 0x48, 0xb, 0x12, 0x6a, 0xa, 0x19, 0x53, 
			0x2e, 0x60, 0x74, 0x30, 0x2d, 0x20, 0xf, 0x29, 0x26, 0x38, 0x71, 0x3d, 0x7a, 0x64, 0x4f, 0xc, 
			0x32, 0x79, 0x3f, 0x58, 0x2a, 0x3b, 0x27, 0x2, 0x55, 0x59, 0x3a, 0x49, 0x4d, 0x67, 0x23, 0x41
		};

		static unsigned char tmp[Key_size];
		memcpy( tmp, key_, sizeof(tmp) );
		if ( ENCRYPT == t )
		{
			for ( size_t i = 0; i < Rounds; ++i )
			{
				this->subkey_select( tmp, this->round_key_[i], Select_table );
				Bit_permutation<16>( tmp, tmp, Key_Ptable );
				if ( i != 1 && i != 4 && i != 5 && i != 7 )
					tmp[0] ^= 0x4D;
			}
		}
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PUFFIN<Block_size, Key_size, Rounds>::subkey_select ( key_block key, block sub_key, const unsigned char (&table)[Block_size * 8] )
	{
		unsigned char tmp[sizeof(table)];

		
		for ( size_t i = 0; i < sizeof(table); ++i )
		{
			tmp[i] = static_cast<unsigned char>( ( (key[ table[i] / 8 ] >> (7 - table[i] % 8) ) & 0x01 ) );
		}

		for ( size_t i = 0; i < sizeof(table) / 8; ++i )
		{
			sub_key[i] = 0;
			for( size_t j = 0; j < 8; ++j )
			{
				sub_key[i] <<= 1;
				sub_key[i] |= tmp[i * 8 + j];
			}
		}
	}


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PUFFIN<Block_size, Key_size, Rounds>::permutation( block in )
	{
		const static unsigned char Ptable[] = 
		{
			0xc, 0x1, 0x3b, 0x31, 0x32, 0x1a, 0x9, 0x23, 0x18, 0x6, 0x1f, 0x3c, 0x0, 0x30, 0x2e, 0x12, 
			0x21, 0x34, 0xf, 0x15, 0x38, 0x13, 0x2f, 0x28, 0x8, 0x33, 0x5, 0x1e, 0x3d, 0x1d, 0x1b, 0xa, 
			0x24, 0x10, 0x39, 0x7, 0x20, 0x2b, 0x2d, 0x3a, 0x17, 0x36, 0x3e, 0x25, 0x37, 0x26, 0xe, 0x16, 
			0xd, 0x3, 0x4, 0x19, 0x11, 0x35, 0x29, 0x2c, 0x14, 0x22, 0x27, 0x2, 0xb, 0x1c, 0x2a, 0x3f
		};
		Bit_permutation<8>( in, in, Ptable );
	}	

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PUFFIN<Block_size, Key_size, Rounds>::substitution( block in )
	{
		const static unsigned char Sbox[16] = 
		{
			0xD, 0x7, 0x3, 0x2, 0x09, 0xA, 0xC, 0x1, 0xF, 0x4, 0x5, 0xE, 0x6, 0x0, 0xB, 0x8
		};
		
		for ( size_t i = 0; i < Block_size; ++i )
		{
			in[i] = Sbox[in[i] & 0x0F] + 0x10 * Sbox[in[i] >> 4];
		}
	}
} // End of Namespace

#endif