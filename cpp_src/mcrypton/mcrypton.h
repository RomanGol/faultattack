#ifndef _MCRYPTON_H_
#define _MCRYPTON_H_

#include <string>
#include "spn.h"
#include "util.h"
#include "random_seq.h"

#define PRINT 0
const static unsigned short ERROR_PLACE = 2;

/* ------------------------ Declaration	------------------------ */
namespace triod
{
	static unsigned char Ptable[64] = 
	{
		0,16,32,48,1,17,33,49,2,18,34,50,3,19,35,51,
		4,20,36,52,5,21,37,53,6,22,38,54,7,23,39,55,
		8,24,40,56,9,25,41,57,10,26,42,58,11,27,43,59,
		12,28,44,60,13,29,45,61,14,30,46,62,15,31,47,63 
	};

	static unsigned char Sbox[4][16] = 
	{
		{4, 15, 3, 8, 13, 10, 12, 0, 11, 5, 7, 14, 2, 6, 1, 9},
		{1, 12, 7, 10, 6, 13, 5, 3, 15, 11, 2, 0, 8, 4, 9, 14},
		{7, 14, 12, 2, 0, 9, 13, 10, 3, 15, 5, 8, 6, 4, 11, 1},
		{11, 0, 10, 7, 13, 6, 4, 2, 12, 14, 3, 9, 1, 5, 15, 8},
	};


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class MCRYPTON : public SPN<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)	[Block_size];
		typedef unsigned char			(&block)		[Block_size];
		typedef const unsigned char		(&key_block)	[Key_size];

		void encrypt	( const_block plain, block cipher, key_block key );
		void decrypt	( const_block cipher, block plain, key_block key );

		void encrypt_f	( const_block plain, block subkey, key_block key );

		void permutation_reverse	( block in );
		void permutation	( block in );
	protected:
		void key_addition	( block input, const_block key );
		
		void substitution	( block in );
		void key_schedule	( Op_type t );
		
		unsigned short	round_counter_;
		unsigned char	key_[Key_size];
		unsigned char	round_key_[Rounds + 1][Block_size];	
	};
} // End of Namespace


/* ------------------------ Implementation 	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MCRYPTON<Block_size, Key_size, Rounds>::encrypt( const_block plain, block cipher, key_block key )
	{
		this->set_key( key );
		memcpy( cipher, plain, Block_size );
		key_schedule( ENCRYPT );
		
		key_addition( cipher, this->round_key_[0] );

		for ( this->round_counter_ = 0; this->round_counter_ < Rounds; ++this->round_counter_ )
		{
			this->substitution( cipher );
#if PRINT
			if ( Rounds - 2 == round_counter_ )
				Print_block<Block_size>( cipher );
#endif
			this->permutation( cipher );

#if PRINT
			if ( Rounds - 2 == round_counter_ )
				Print_block<Block_size>( cipher );
#endif

			key_addition( cipher, this->round_key_[this->round_counter_ + 1] );
		}
		this->round_counter_ = 0;
	}
	

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MCRYPTON<Block_size, Key_size, Rounds>::encrypt_f( const_block plain, block cipher, key_block key )
	{
		static unsigned char choose[] = { 0x0F, 0xF0 };
		this->set_key( key );
		memcpy( cipher, plain, Block_size );
		key_schedule( ENCRYPT );
		
		key_addition( cipher, this->round_key_[0] );

		for ( this->round_counter_ = 0; this->round_counter_ < Rounds; ++this->round_counter_ )
		{
			this->substitution( cipher );

			if ( Rounds - ERROR_PLACE == round_counter_ )
			{
				unsigned char r = ( rand()  & choose[rand() % 2] );
				while ( 0 == r )
					r = ( rand()  & choose[rand() % 2] );
				cipher[rand() % Block_size] ^= r;
			}

			#if PRINT
			if ( Rounds - 2 == round_counter_ )
				Print_block<Block_size>( cipher );
			#endif

			this->permutation( cipher );

			#if PRINT
			if ( Rounds - 2 == round_counter_ )
				Print_block<Block_size>( cipher );
			#endif

			key_addition( cipher, this->round_key_[this->round_counter_ + 1] );
		}
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MCRYPTON<Block_size, Key_size, Rounds>::decrypt( const_block cipher, block plain, key_block key )
	{
		// no implementation
	}	
	

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MCRYPTON<Block_size, Key_size, Rounds>::key_addition ( block input, const_block key )
	{
		for ( size_t i = 0; i < Block_size; ++i )
			input[i] ^= key[i];
	}


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MCRYPTON<Block_size, Key_size, Rounds>::key_schedule( Op_type t )
	{
		size_t counter = 0;
		if ( ENCRYPT == t )
		{
			for ( size_t i = 0; i < Rounds + 1; ++i )
				memcpy( round_key_[i], Random_sequence + i * Block_size, Block_size );
		}
	}
	

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MCRYPTON<Block_size, Key_size, Rounds>::permutation_reverse( block in )
	{
		const static unsigned short mask[] = { 0x0E, 0x0D, 0x0B, 0x07  };

		static unsigned char ex[] = { 0x0, 0x4, 0x8, 0xC, 0x1, 0x5, 0x9, 0xD, 0x2, 0x6, 0xA, 0xE, 0x3, 0x7, 0xB, 0xF  };
		static unsigned char tmp[] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0  };

		unsigned char matrix[Block_size * 2];

		for ( size_t j = 0; j < Block_size; ++j )
		{
			matrix[j * 2 + 0] = (in[j] & 0x0F);
			matrix[j * 2 + 1] = (in[j] >> 4);
		}

		unsigned char b[Block_size * 2];
		
		for ( size_t i = 0; i < 4; ++i )
		{
			for ( size_t j = 0; j < 4; ++j )
			{
				b[i * 4 + j] = 0;
				for ( size_t k = 0; k < 4; ++k )
				{
					b[i * 4 + j] ^= ( mask[(i + j + k) % 4] & matrix[i * 4 + k] );
				}
			}
		}

		// Column to Row Transposition
		for ( size_t j = 0; j < Block_size * 2; ++j )
			tmp[j] = b[ ex[j] ];
		for ( size_t j = 0; j < Block_size * 2; ++j )
			b[j] = tmp[j];

		for ( size_t i = 0; i < Block_size; ++i )
			in[i] = b[i * 2] + b[i * 2 + 1] * 0x10;
	}


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MCRYPTON<Block_size, Key_size, Rounds>::permutation( block in )
	{
		const static unsigned short mask[] = { 0x0E, 0x0D, 0x0B, 0x07  };

		unsigned short matrix[Block_size * 2];

		for ( size_t j = 0; j < Block_size; ++j )
		{
			matrix[j * 2 + 0] = (in[j] & 0x0F);
			matrix[j * 2 + 1] = (in[j] >> 4);
		}

		unsigned short a[Block_size * 2];
		unsigned short b[Block_size * 2];
		
		for ( size_t j = 0; j < Block_size / 2; ++j )
		{
			a[j * 4 + 0] = matrix[j + 0x0];
			a[j * 4 + 1] = matrix[j + 0x4];
			a[j * 4 + 2] = matrix[j + 0x8];
			a[j * 4 + 3] = matrix[j + 0xC];
		}

		for ( size_t i = 0; i < 4; ++i )
		{
			for ( size_t j = 0; j < 4; ++j )
			{
				b[i * 4 + j] = 0;
				for ( size_t k = 0; k < 4; ++k )
				{
					b[i * 4 + j] ^= ( mask[(i + j + k) % 4] & a[i * 4 + k] );
				}
			}
		}

		for ( size_t i = 0; i < Block_size; ++i )
		{
			in[i] = b[i * 2] + b[i * 2 + 1] * 0x10;
		}
	}


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void MCRYPTON<Block_size, Key_size, Rounds>::substitution( block in )
	{
		in[0] = Sbox[0][in[0] & 0x0F] + 0x10 * Sbox[1][in[0] >> 4];
		in[1] = Sbox[2][in[1] & 0x0F] + 0x10 * Sbox[3][in[1] >> 4];
		in[2] = Sbox[1][in[2] & 0x0F] + 0x10 * Sbox[2][in[2] >> 4];
		in[3] = Sbox[3][in[3] & 0x0F] + 0x10 * Sbox[0][in[3] >> 4];
		in[4] = Sbox[2][in[4] & 0x0F] + 0x10 * Sbox[3][in[4] >> 4];
		in[5] = Sbox[0][in[5] & 0x0F] + 0x10 * Sbox[1][in[5] >> 4];
		in[6] = Sbox[3][in[6] & 0x0F] + 0x10 * Sbox[0][in[6] >> 4];
		in[7] = Sbox[1][in[7] & 0x0F] + 0x10 * Sbox[2][in[7] >> 4];
	}
} // End of Namespace

#endif
