/*

Filename:		present_fault.h
Function:		Block cipher PRESENT fault attacker
Maintainer:		RomanGol  romangoliard@gmail.com
Last Modify:	2011-11
Comment:		
				

*/


#ifndef _PRESENT_FAULT_H_
#define _PRESENT_FAULT_H_

#include "present.h"

/* ------------------------ Declaration	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class PRESENT_FAULT : public PRESENT<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)	[Block_size];
		typedef unsigned char			(&block)		[Block_size];
		typedef const unsigned char		(&key_block)	[Key_size];

		void fncrypt ( const_block plain, block cipher, key_block key, const size_t e_round, const size_t e_pos, const unsigned char err );
		
		void multi_fncrypt ( const_block plain, block cipher, key_block key, const size_t e_round, const size_t e_pos, const unsigned char err );
		
	};
} // End of Namespace


/* ------------------------ Implementation 	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRESENT_FAULT<Block_size, Key_size, Rounds>::fncrypt( const_block plain, block cipher, key_block key, 
												const size_t e_round, const size_t e_pos, const unsigned char err )
	{
		memcpy( cipher, plain, Block_size );
		this->key_schedule( ENCRYPT, key );
	
		for ( this->round_counter_ = 0; this->round_counter_ < Rounds; ++this->round_counter_ )
		{
			// simulate the error
			if ( this->round_counter_ == e_round )
			{
				cipher[e_pos / 2] ^= (e_pos % 2 == 1 ? err << 4 : err);
			}
			// simulate the error


			this->key_addition( cipher, this->round_key_[this->round_counter_] );
			this->substitution( cipher );
			this->permutation( cipher );
		}

		
		this->key_addition( cipher, this->round_key_[Rounds] );
		this->round_counter_ = 0;
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRESENT_FAULT<Block_size, Key_size, Rounds>::multi_fncrypt( const_block plain, block cipher, key_block key, 
												const size_t e_round, const size_t e_pos, const unsigned char err )
	{
		memcpy( cipher, plain, Block_size );
		this->key_schedule( ENCRYPT, key );
	
		for ( this->round_counter_ = 0; this->round_counter_ < Rounds; ++this->round_counter_ )
		{
			// simulate the error
			if ( this->round_counter_ == e_round )
			{
				cipher[e_pos / 2] ^= (e_pos % 2 == 1 ? err << 4 : err);

				int m[3];
				m[0] = 1 + rand() % 15;
				m[1] = 1 + rand() % 15;
				m[2] = 1 + rand() % 15;
			
				while ( m[1] == m[0] || m[1] == m[2] || m[2] == m[0] )
				{
					m[1] =  1 + rand() % 15;
					m[2] =  1 + rand() % 15;
				}

				int new_e_pos = (e_pos + m[0]) % 16;
				cipher[new_e_pos / 2] ^= (new_e_pos % 2 == 1 ? err << 4 : err);
				new_e_pos = (e_pos + m[1]) % 16;
				cipher[new_e_pos / 2] ^= (new_e_pos % 2 == 1 ? err << 4 : err);
				new_e_pos = (e_pos + m[2]) % 16;
				cipher[new_e_pos / 2] ^= (new_e_pos % 2 == 1 ? err << 4 : err);				
			}
			// simulate the error

			this->key_addition( cipher, this->round_key_[this->round_counter_] );
			this->substitution( cipher );
			this->permutation( cipher );
		}

		
		this->key_addition( cipher, this->round_key_[Rounds] );
		this->round_counter_ = 0;
	}


} // End of Namespace

#endif
