/*

Filename:		present.h
Function:		Block cipher PRESENT
Maintainer:		RomanGol  romangoliard@gmail.com
Last Modify:	2011-11
Comment:		due to template��cannot compiled seperately ����
				2011-03��add compilation guard 

*/


#ifndef _PRESENT_H_
#define _PRESENT_H_

#include <string>
#include "../spn.h"
#include "../util_le.h"
#include "present_key.h"

/* ------------------------ Declaration	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class PRESENT : public SPN<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)	[Block_size];
		typedef unsigned char			(&block)		[Block_size];
		typedef const unsigned char		(&key_block)	[Key_size];

		PRESENT();
		void encrypt	( const_block plain, block cipher, key_block key );
		void decrypt	( const_block cipher, block plain, key_block key );
		
	protected:
		void key_addition	( block input, const_block key );
		void permutation	( block in );
		void substitution	( block in );
		void key_schedule	( Op_type t, key_block key );

		uint16_t	round_counter_;
		uint8_t		key_[Key_size];
		uint8_t		round_key_[Rounds + 1][Block_size];	
	};
} // End of Namespace


/* ------------------------ Implementation 	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	PRESENT<Block_size, Key_size, Rounds>::PRESENT()
	{
		char guard[(Key_size == 10 || Key_size == 16) ? 1 : -1] = { 0 };

		char guard_a[(Rounds == 31) ? 1 : -1] = { 0 };
 	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRESENT<Block_size, Key_size, Rounds>::encrypt( const_block plain, block cipher, key_block key )
	{
		memcpy( cipher, plain, Block_size );
		this->key_schedule( ENCRYPT, key );
		
		for ( round_counter_ = 0; round_counter_ < Rounds; ++round_counter_ )
		{
			key_addition( cipher, round_key_[round_counter_] );
			
			//if ( round_counter_ == Rounds - 1 ) triod::split_and_print_block<Block_size>( cipher, 4 );
			this->substitution( cipher );
			this->permutation( cipher );
		}
		
		key_addition( cipher, this->round_key_[Rounds] );
		//triod::split_and_print_block<Block_size>( cipher, 4 );
		this->round_counter_ = 0;
 	}
	

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRESENT<Block_size, Key_size, Rounds>::decrypt( const_block cipher, block plain, key_block key )
	{
		// no implementation
	}	
	

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRESENT<Block_size, Key_size, Rounds>::key_addition ( block input, const_block key )
	{
		for ( size_t i = 0; i < Block_size; ++i )
			input[i] ^= key[i];
	}


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRESENT<Block_size, Key_size, Rounds>::key_schedule( Op_type t, key_block key )
	{
		PRESENT_Key_scheduler<Block_size, Key_size, Rounds> ks;
		ks.set_key( key );
		ks.key_schedule( t );

		for ( size_t i = 0; i < Rounds + 1; ++i )
			ks.fill_round_key( i, round_key_[i] );
	}
	
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRESENT<Block_size, Key_size, Rounds>::permutation( block in )
	{
		const static unsigned char Ptable[] = 
		{
			0,16,32,48,1,17,33,49,2,18,34,50,3,19,35,51,
			4,20,36,52,5,21,37,53,6,22,38,54,7,23,39,55,
			8,24,40,56,9,25,41,57,10,26,42,58,11,27,43,59,
			12,28,44,60,13,29,45,61,14,30,46,62,15,31,47,63 
		};

		Bit_permutation<Block_size>( in, in, Ptable );
	}	

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRESENT<Block_size, Key_size, Rounds>::substitution( block in )
	{
		const static unsigned char Sbox[] = 
		{
			0x0c, 0x05, 0x06, 0x0b, 0x09, 0x00, 0x0A, 0x0D,
			0x03, 0x0e, 0x0f, 0x08, 0x04, 0x07, 0x01, 0x02
		};		

		for ( size_t i = 0; i < Block_size; ++i )
		{
			in[i] = Sbox[in[i] & 0x0F] + 0x10 * Sbox[in[i] >> 4];
		}
	}
} // End of Namespace

#endif
