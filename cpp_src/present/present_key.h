/*

Filename:	present_key.h
Function:		PRESENT Key Scheduler
Maintainer:	RomanGol  romangoliard@gmail.com
Last Modify:	2010-08
Comment:		

*/

#ifndef _PRESENT_KEYSCHEDR_H_
#define _PRESENT_KEYSCHEDR_H_

#include "../keyschedr_spn.h"

/* ------------------------ Declaration	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class PRESENT_Key_scheduler : public Key_scheduler<Block_size, Key_size, Rounds>
	{
	public:
		typedef const unsigned char		(&const_block)	[Block_size];
		typedef unsigned char			(&block)		[Block_size];
		typedef const unsigned char		(&key_block)	[Key_size];

		void fill_round_key	( size_t nth_round, block round_key );
		void set_key		( key_block key );
		void key_schedule	( Op_type t );

	protected:
		unsigned char	key_[Key_size];
		unsigned char	round_key_[Rounds + 1][Block_size];		
	}; // End of class SPN


	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRESENT_Key_scheduler<Block_size, Key_size, Rounds>::key_schedule( Op_type t )
	{
		const static unsigned char Sbox[] = 
		{
			0x0c, 0x05, 0x06, 0x0b, 0x09, 0x00, 0x0A, 0x0D,
			0x03, 0x0e, 0x0f, 0x08, 0x04, 0x07, 0x01, 0x02
		};		

		if ( ENCRYPT == t )
		{
			if ( Key_size == 10 )
			{
				for ( size_t round_counter = 0; round_counter < Rounds; ++round_counter )
				{
					memcpy( round_key_[round_counter], key_ + 2, Block_size );
					Rotate_block<Key_size>( LEFT, key_, 61 );
					key_[9] = Sbox[ key_[9] >> 4 ] * 0x10 + (key_[9] & 0x0F);
					key_[2] ^= ( ( (round_counter + 1) >> 1 ) & 0x0F );
					key_[1] ^= ( ( (round_counter + 1) << 7 ) & 0x80 );
				}
				memcpy( round_key_[Rounds], key_ + 2, Block_size );
			}
			if ( Key_size == 16 )
			{
				for ( size_t round_counter = 0; round_counter < Rounds; ++round_counter )
				{
					memcpy( round_key_[round_counter], key_ + 8, Block_size );
					Rotate_block<Key_size>( LEFT, key_, 61 );
					key_[15] = ( Sbox[ key_[15] >> 4 ] << 4 ) + Sbox[ key_[15] & 0x0F ];
					key_[8] ^= ( ( (round_counter + 1) >> 2 ) & 0x07 );
					key_[7] ^= ( ( (round_counter + 1) << 6 ) & 0xC0 );
				}
				memcpy( round_key_[Rounds], key_ + 8, Block_size );
			}
		}
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRESENT_Key_scheduler<Block_size, Key_size, Rounds>::set_key( key_block key )
	{
		for ( size_t i = 0; i < Key_size; ++i )
			key_[i] = key[i];
	}

	template<size_t Block_size, size_t Key_size, size_t Rounds>
	void PRESENT_Key_scheduler<Block_size, Key_size, Rounds>::fill_round_key( size_t nth_round, block round_key )
	{
		memcpy( round_key, round_key_[nth_round], Block_size );
	}

} // End of Namespace




#endif
