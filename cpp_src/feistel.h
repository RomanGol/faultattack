/*

Filename:		feistel.h
Function:		feistel abstract superclass
Maintainer:		RomanGol  romangoliard@gmail.com
Last Modify:	2011-11
Comment:		only declaration��no data member

*/

#ifndef _FEISTEL_H_
#define _FEISTEL_H_

/* ------------------------ Declaration	------------------------ */
namespace triod
{
	template<size_t Block_size, size_t Key_size, size_t Rounds>
	class FEISTEL
	{
	public:
		typedef const unsigned char		(&const_block)		[Block_size];
		typedef unsigned char			(&block)			[Block_size];
		typedef const unsigned char		(&key_block)		[Key_size];
		typedef const unsigned char		(&const_half_block)	[Block_size / 2];
		typedef unsigned char			(&half_block)		[Block_size / 2];

		virtual void encrypt( const_block plain, block cipher, key_block key ) = 0;
		virtual void decrypt( const_block cipher, block plain, key_block key ) = 0;

	protected:
		virtual void key_schedule	( Op_type t, key_block key )			= 0;
		virtual void round_func		( half_block left, half_block right )	= 0;
		
	}; // End of class FEISTEL
	
} // End of Namespace




#endif