def diff_pr ( diff_in, sbox_pr ):
	prb = 1.0
	table = ["000","100","010","110","001","101","011","111"]
	
	for i in range( len(table[0]) ):
		if table[diff_in][i] == '1':
			prb *= sbox_pr[i]
		else:
			prb *= (1 - sbox_pr[i])
	return prb

def diff_io( diff_in, pr ):
	table = ["000","100","010","110","001","101","011","111"]
	sbox = [0, 1, 3, 6, 7, 4, 5, 2]
	# [0, 1, 3, 6, 7, 4, 5, 2],
	# [0, 1, 7, 4, 3, 6, 5, 2],
	# [0, 3, 1, 6, 7, 5, 4, 2],
	# [0, 7, 3, 5, 1, 4, 6, 2]
	
	counter = [0 for i in range( len(table[0]) )] # count each bit of sbox

	for i in range( len(sbox) ):
		s = table[ sbox[i] ^ sbox[i ^ diff_in] ]
		for j in range( len(s) ):
			if '1' == s[j]:
				counter[j] += 1
	for i in range( len(table[0]) ):
		counter[i] = counter[i] * pr / len(sbox)
	return counter

def perm( t_in ):
	ptab = [0, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34, 37, 40, 43, 46, 2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35, 38, 41, 44, 47]
	out = [0.0 for i in range( len(t_in)  )]
	for i in range( len(t_in) ):
		out[ ptab[i] ] = t_in[i]
	t_in = out
	return t_in

def print_prb_tab( pr_tab ):
	bit_of_sbox = 3
	block_bits = 48
	for i in range(block_bits // bit_of_sbox):
		for j in range(bit_of_sbox):
			print ( format(pr_tab[i * bit_of_sbox + j], "0.3f" ), end=" "),
		print()
	print ("--------------")

def sbox_pr( bits_of_sbox, diff_lst, diff_pr_list ):
	nibb_pr = [0 for i in range(bits_of_sbox)]
	for k in range( len(diff_lst) ):
		c = diff_io( diff_lst[k], diff_pr_list[k] )
		for i in range( bits_of_sbox ):
			nibb_pr[i] += c[i]

	return nibb_pr
###################### main ######################
bit_of_sbox = 3
len_of_sbox = 8
block_bits = 48
pr_tab = [1.0 / 28 for i in range(block_bits)]

for times in range(2):
	perm(pr_tab)
	
	for sbx in range(block_bits // bit_of_sbox):
		diff = [1,2,4] # single bit input diff affects four sboxes
		diff_pr_lst = pr_tab[sbx * bit_of_sbox: sbx * bit_of_sbox + bit_of_sbox]
		nibb_pr = sbox_pr( bit_of_sbox, diff, diff_pr_lst )
		pr_tab[sbx * bit_of_sbox: sbx * bit_of_sbox + bit_of_sbox] = nibb_pr
	print_prb_tab( pr_tab ) 

perm(pr_tab)
for sbx in range(block_bits // bit_of_sbox): # random input diff affects sixteen sboxes except 0x07
	sbox_pr = pr_tab[ sbx * bit_of_sbox : sbx * bit_of_sbox + bit_of_sbox ]
	nibb_pr = [0, 0, 0]
	for dif in range( 1, len_of_sbox - 1 ):
		p = diff_pr( dif, sbox_pr )
		c = diff_io( dif, p )
		for i in range( bit_of_sbox ):
			nibb_pr[i] += c[i]

	pr_tab[sbx * bit_of_sbox: sbx * bit_of_sbox + bit_of_sbox] = nibb_pr

print_prb_tab( pr_tab ) 

#######################################################################################

for times in range (0):
	perm(pr_tab)
	for sbx in range(block_bits // bit_of_sbox): # random input diff affects sixteen sboxes
		sbox_pr = pr_tab[sbx * bit_of_sbox : sbx * bit_of_sbox + bit_of_sbox ]
		nibb_pr = [0, 0, 0]
		for dif in range( 1, len_of_sbox ):
			p = diff_pr(dif, sbox_pr)
			c = diff_io( dif, p )
			for i in range( bit_of_sbox ):
				nibb_pr[i] += c[i]
		pr_tab[sbx * bit_of_sbox: sbx * bit_of_sbox + bit_of_sbox] = nibb_pr

	print_prb_tab( pr_tab ) 
