#ptab = [0 for i in range(48)]
#for i in range(47):
#	ptab[i] = 3 * i % 47
#ptab[47] = 47
#print(ptab)


def perm(table, ptab):
	xtable = [0 for i in range( len(table) )]
	for i in range( len(table) ):
		xtable[ ptab[i] ] = table[i]
	for i in range( len(table) ):
		table[i] = xtable[i]

ptab = [0, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34, 37, 40, 43, 46, 2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35, 38, 41, 44, 47]

for i in range(48):
	table = [0 for i in range(48)]
	table[i] = 1
	for j in range(3):
		xtable = [0 for n in range(48)]
		for k in range(48):
			if table[k] == 1:
				xtable[k // 3 * 3] = 1
				xtable[k // 3 * 3 + 1] = 1
				xtable[k // 3 * 3 + 2] = 1
		table = xtable
		perm( table, ptab )
	print ( table )

