data = open("test.txt", "r")

stat = [ [0 for i in range(16)] for j in range(16) ]

s = data.readline() 
print ( s )

	
while s != "":
	s = data.readline() 
	l = s.split()
	if ( len(l) == 16 ):
		for i in range(16):
			stat[i][ int( l[i], 16 ) ] += 1


for e in stat:
	sum = 0
	for f in e:
		sum += f
	for f in e:
		print( "{:.4f}".format(f*100.0/sum), sep="", end=" " )
	print( "" )

data.close()
