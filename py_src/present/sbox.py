def diff_pr ( diff_in, sbox_pr ):
	prb = 1.0
	table = ["0000","1000","0100","1100","0010","1010","0110","1110","0001","1001","0101","1101","0011","1011","0111","1111"]
	for i in range(4):
		if table[diff_in][i] == '1':
			prb *= sbox_pr[i]
		else:
			prb *= (1 - sbox_pr[i])
	return prb

def uniform( tb ):
	sum = 0.0
	for i in tb:
		sum += i
	for i in range( len(tb) ):
		tb[i] = tb[i] * 1.0 / sum
	return tb

def diff_io( diff_in, pr ):
	table = ["0000","1000","0100","1100","0010","1010","0110","1110","0001","1001","0101","1101","0011","1011","0111","1111"]
	sbox = [0x0c, 0x05, 0x06, 0x0b, 0x09, 0x00, 0x0A, 0x0D,0x03, 0x0e,0x0f, 0x08, 0x04, 0x07, 0x01, 0x02]
	counter = [0 for i in range( len(table[0]) )] # count each bit of sbox
	for i in range( len(sbox) ):
		s = table[ sbox[i] ^ sbox[i ^ diff_in] ]
		for j in range( len(s) ):
			if '1' == s[j]:
				counter[j] += 1
	for i in range( len(table[0]) ):
		counter[i] = counter[i] * pr / len(sbox)
	return counter

def perm( t_in ):
	ptab = [0,16,32,48,1,17,33,49,2,18,34,50,3,19,35,51,4,20,36,52,5,21,37,53,6,22,38,54,7,23,39,55,8,24,40,56,9,25,41,57,10,26,42,58,11,27,43,59,12,28,44,60,13,29,45,61,14,30,46,62,15,31,47,63]
	out = [0.0 for i in range( len(t_in)  )]
	for i in range( len(t_in) ):
		out[ ptab[i] ] = t_in[i]
	for i in range( len(t_in) ):
		t_in[i] = out[i]
	return out

def print_table(pr_tab):
	for i in range(16):
		for j in range(4):
			print ( format(pr_tab[i*4 + j], "0.3f" ), end=" "),
		print()
	print ("--------------")

###################### main ######################
block_bits = 64
pr_tab = [1.0 / 30.0 for i in range(block_bits)]

for times in range(2):
	# single bit input diff affects four sboxes
	for sbx in range(16):
		nibb_pr = [0, 0, 0, 0]
		diff = [1,2,4,8]
		for k in range( len(diff) ):
			c = diff_io( diff[k], pr_tab[sbx * 4 + k] )
			for i in range( len(nibb_pr) ):
				nibb_pr[i] += c[i]
		pr_tab[sbx * 4:sbx * 4 + 4] = nibb_pr

	perm(pr_tab)
	print_table(pr_tab)

	
for times in range (4):
# random input diff affects sixteen sboxes
	for sbx in range(16):
		nibb_pr = [0, 0, 0, 0]
		sbox_pr = pr_tab[sbx * 4: sbx * 4 + 4]
		for dif in range( 1, 16 ):
			p = diff_pr(dif, sbox_pr)
			c = diff_io( dif, p )
			for i in range( len(nibb_pr) ):
				nibb_pr[i] += c[i]
		pr_tab[sbx * 4:sbx * 4 + 4] = nibb_pr

	perm(pr_tab)
	print_table(pr_tab)

